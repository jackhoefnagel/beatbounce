﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(DynamicPlaylistCreator))]
[CanEditMultipleObjects]
public class EditorDynamicPlaylistCreator : Editor
{

    private DynamicPlaylistCreator playlistCreator;

    private bool collapsed;
    private int arraySize;

    //###############################################################################################
    //Start
    void OnEnable()
    {
        playlistCreator = target as DynamicPlaylistCreator;
        playlistCreator.CreatePlaylist();
    }
    //###############################################################################################
    //Update
    public override void OnInspectorGUI()
    {
        playlistCreator.playlistName = EditorGUILayout.TextField("Playlist Name", playlistCreator.playlistName);

        playlistCreator.matchGenre = EditorGUILayout.Toggle("Match Genre", playlistCreator.matchGenre);
        if (playlistCreator.matchGenre)
        {
            EditorGUILayout.BeginVertical();
            playlistCreator.currentGenre = (SongInfos.Genres)EditorGUILayout.EnumPopup("Genre", playlistCreator.currentGenre);

            EditorGUILayout.EndVertical();
        }

         playlistCreator.matchBPM = EditorGUILayout.Toggle("Match BPM", playlistCreator.matchBPM);
        if (playlistCreator.matchBPM)
        {
            EditorGUILayout.BeginVertical();
            playlistCreator.bPMRange = (Vector2)EditorGUILayout.Vector2Field("BPM Range", playlistCreator.bPMRange);

            EditorGUILayout.EndVertical();
        }

        playlistCreator.matchMood = EditorGUILayout.Toggle("Match Mood", playlistCreator.matchMood);
        if (playlistCreator.matchMood)
        {
            EditorGUILayout.BeginVertical();
            playlistCreator.currentMood = (SongInfos.SongMoods)EditorGUILayout.EnumPopup("Mood", playlistCreator.currentMood);

            EditorGUILayout.EndVertical();
        }

        if (GUILayout.Button("Refresh Playlist"))
        {
            playlistCreator.CreatePlaylist();
        }

            //playlistCreator.nextObj = (MenueElement)EditorGUILayout.ObjectField("Next Menuepoint", playlistCreator.nextObj, typeof(MenueElement), true);
            //playlistCreator.previousObj = (MenueElement)EditorGUILayout.ObjectField("Previous Menuepoint", playlistCreator.previousObj, typeof(MenueElement), true);
            ////    sprite.loopAnimation = EditorGUILayout.Toggle("Loop", sprite.loopAnimation);
            ////    sprite.bounceAnimation = EditorGUILayout.Toggle("Bounce", sprite.bounceAnimation);
            ////    sprite.autoplayAnimation = EditorGUILayout.Toggle("Autoplay", sprite.autoplayAnimation);
            ////    sprite.animationDuration = EditorGUILayout.FloatField("Duration (sec)", sprite.animationDuration);
            //GUILayout.Label("Surrounding Objects Positions");
            //playlistCreator.surroundingObjPosition[0] = EditorGUILayout.Toggle("Previous", playlistCreator.surroundingObjPosition[0]);
            //playlistCreator.surroundingObjPosition[1] = EditorGUILayout.Toggle("Next", playlistCreator.surroundingObjPosition[1]);

            //EditorGUILayout.EndVertical();
            ////############################################################
            //EditorGUILayout.Space();
            ////############################################################
            //EditorGUILayout.BeginVertical();

            //playlistCreator.graphicsUnselected = (GameObject)EditorGUILayout.ObjectField("Graphics unselected", playlistCreator.graphicsUnselected, typeof(GameObject), true);
            //playlistCreator.graphicsSelected = (GameObject)EditorGUILayout.ObjectField("Graphcis selected", playlistCreator.graphicsSelected, typeof(GameObject), true);

            //EditorGUILayout.EndVertical();
            ////############################################################
            //EditorGUILayout.Space();
            ////############################################################

            //playlistCreator.switchEvent = (CEventType)EditorGUILayout.EnumPopup("Switch Event", playlistCreator.switchEvent);

            //playlistCreator.AEvent = (CEventType)EditorGUILayout.EnumPopup("A-Button Event", playlistCreator.AEvent);
            //playlistCreator.BEvent = (CEventType)EditorGUILayout.EnumPopup("B-Button Event", playlistCreator.BEvent);

            //playlistCreator.currentOption = EditorGUILayout.IntField("Current option nr.", playlistCreator.currentOption);

            serializedObject.Update();
            //EditorGUIUtility.LookLikeControls();
        SerializedProperty tps = serializedObject.FindProperty("playlistSongs");
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(tps, true);
        if (EditorGUI.EndChangeCheck())
            serializedObject.ApplyModifiedProperties();
        EditorGUIUtility.LookLikeControls();


        

        // Show default inspector property editor
        //DrawDefaultInspector();
    }

}


