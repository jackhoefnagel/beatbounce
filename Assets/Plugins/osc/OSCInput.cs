using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Osc),typeof(UDPPacketIO))]
public class OSCInput : MonoBehaviour {

	Osc oscHandler;
	internal bool isConnected {get; private set;}
	internal static bool[] inputs = new bool[16];

    static OSCInput me;

	void Start ()
	{
        if (me != null)
        {
            Debug.LogError("More than one OCSINput");
        }
        me = this;

		UDPPacketIO udp = GetComponent<UDPPacketIO>();
		
		udp.init( "127.0.0.1", /*"22000, 21000);*/ 12000, 3200 );
		
		oscHandler = GetComponent<Osc>();
		oscHandler.init( udp );
        oscHandler.SetAllMessageHandler(ReceiveMessage);
		
		isConnected = true;
	}

    void Update()
    {
        print(GetDir(0));
    }

    public void ReceiveMessage(OscMessage oscMessage)
	{
        string str = "";
        for (int i = 0; i < oscMessage.Values.Count; i++)
        {
            inputs[i] = (int)oscMessage.Values[i] > 0;
            str += inputs[i] ? "+" : "-";
        }
        print(str);
	}

    public static Vector2 GetDir(int controllerId)
    {
        if (controllerId == 4) {
            Debug.Log("horizontal player: "+(controllerId + 1)+" "+Input.GetAxisRaw("HorizontalPlayer" + (controllerId + 1)));
            return new Vector2(Input.GetAxisRaw("HorizontalPlayer" + (controllerId + 1)), Input.GetAxisRaw("VerticalPlayer" + (controllerId + 1)));
        }
        else {
            int left = inputs[controllerId * 4] ? 1 : 0;
            int right = inputs[controllerId * 4 + 1] ? 1 : 0;
            int down = inputs[controllerId * 4 + 2] ? 1 : 0;
            int up = inputs[controllerId * 4 + 3] ? 1 : 0;
            return new Vector2(right - left, up - down);
        }

    }
}
