﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

	public GameObject cameraPrefab;
	[HideInInspector] public GameObject currentCam;
	[HideInInspector] public CameraMoveScript camMoveScript;

	public float screenshakeMagnitude = 0.5f;

	public static CameraManager instance {get; private set;}
	void Awake(){
		instance = this;
	}

	void Start(){


	}

	public void InitCam(){
		if(!currentCam)
		{
			currentCam = (GameObject)Instantiate(cameraPrefab);
			currentCam.name = cameraPrefab.name;
		}
		
		camMoveScript = currentCam.GetComponent<CameraMoveScript>();
		
		camMoveScript.player1 = PlayerManager.instance.player1;
		camMoveScript.player2 = PlayerManager.instance.player2;
		camMoveScript.player3 = PlayerManager.instance.player3;
		camMoveScript.player4 = PlayerManager.instance.player4;
		camMoveScript.GetPlayers();
		//TODO: little bit overkill on getting players for camera centering, 3 places now

		foreach(Transform child in currentCam.transform)
		{
			if(child.tag == "PulseBackgroundObject")
			{
				GraphicsManager.instance.pulseBGGO = child.gameObject;
			}
			else if(child.tag == "BackgroundColorObject")
			{
				GraphicsManager.instance.BGColorGO = child.gameObject;
			}

			GraphicsManager.instance.ColorBGGraphics();
			/*
			else if(child.tag == "BGParticles1")
			{
				GraphicsManager.instance.bgParticlesColor1GO = child.gameObject;
			}
			else if(child.tag == "BGParticles2")
			{
				GraphicsManager.instance.bgParticlesColor2GO = child.gameObject;
			}
			*/
		}

		camMoveScript.InitCam();


		
	}

	public void StopCam(){
		//TODO: stop cam? maybe nice transition when starting again?
		//Destroy(currentCam);

		camMoveScript.StopCam();
	}


	public void DoScreenShake()
	{
		camMoveScript.screenShakeMagnitude = screenshakeMagnitude;
		camMoveScript.DoScreenShake();
	}
}
