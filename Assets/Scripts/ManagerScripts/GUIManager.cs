﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour {

	public static GUIManager instance {get; private set;}	
	void Awake(){
		instance = this;
	}

	private ArrayList P1_ScoreIcons;
	private ArrayList P2_ScoreIcons;
	private ArrayList P3_ScoreIcons;
	private ArrayList P4_ScoreIcons;

	private GameObject InstructionScreen;
    private GameObject WinMessage;
	private GameObject GameOverMessage;
	private GameObject RoundIndicator;
	private GameObject GetReady ;
	//private bool playerHasWon  = false;

    //Sam
    //private MusicManager musicManager;
    //private GameManager gameManager;

    public string detailNextSongSeparator;

    public Text detailNextSong;
    public Text detailNextSongOutline;
    public Text announcement;

    public Text timeText;
    public Text songText;
    public GameObject popUp;
	private bool popUpHidden = true;

    public float popUpTime = 0.0F;
    public float popUpTimeCurrent = 4.0F;

	private bool guiInit = false;

    public GameObject mainUIPrefab;
    private GameObject mainUI;


	void Start()
	{
		P1_ScoreIcons = new ArrayList();
		P1_ScoreIcons.Add(GameObject.Find("score_square1"));
		P1_ScoreIcons.Add(GameObject.Find("score_square2"));
		P1_ScoreIcons.Add(GameObject.Find("score_square3"));
		
		P2_ScoreIcons = new ArrayList();
		P2_ScoreIcons.Add(GameObject.Find("score_two_squares1"));
		P2_ScoreIcons.Add(GameObject.Find("score_two_squares2"));
		P2_ScoreIcons.Add(GameObject.Find("score_two_squares3"));
		
		P3_ScoreIcons = new ArrayList();
		P3_ScoreIcons.Add(GameObject.Find("score_triangles1"));
		P3_ScoreIcons.Add(GameObject.Find("score_triangles2"));
		P3_ScoreIcons.Add(GameObject.Find("score_triangles3"));
		
		P4_ScoreIcons = new ArrayList();
		P4_ScoreIcons.Add(GameObject.Find("score_bricks1"));
		P4_ScoreIcons.Add(GameObject.Find("score_bricks2"));
		P4_ScoreIcons.Add(GameObject.Find("score_bricks3"));

	}

	public void InitGUI()
	{
		Transform GUIObject = CameraManager.instance.currentCam.transform.Find("GUI");
		
		WinMessage = GUIObject.Find("WinMessage").gameObject;
		GameOverMessage = GUIObject.Find("GameOver").gameObject;
		RoundIndicator = GUIObject.Find("RoundIndicator").gameObject;
		GetReady = GUIObject.Find("GetReady").gameObject;
		InstructionScreen = GUIObject.Find("InstructionScreen").gameObject;
		
		WinMessage.GetComponent<Renderer>().enabled = false;
		GameOverMessage.GetComponent<Renderer>().enabled = false;
		RoundIndicator.GetComponent<Renderer>().enabled = false;
		
		DisplayMessage("prestart");
		
		// TODO: show track name
		//RoundIndicator.GetComponent<TextMesh>().text = audio.clip.name;
		
		// change message text colors
		WinMessage.GetComponent<TextMesh>().color = GraphicsManager.instance.playerColorBasedOnBG;
		GameOverMessage.GetComponent<TextMesh>().color = GraphicsManager.instance.playerColorBasedOnBG;
		RoundIndicator.GetComponent<TextMesh>().color = GraphicsManager.instance.playerColorBasedOnBG;
		GetReady.GetComponent<TextMesh>().color = GraphicsManager.instance.playerColorBasedOnBG;
		
		
		//musicManager = GameObject.Find("_MusicManager").GetComponent<MusicManager>(); // sam
		if (mainUI == null)
		{
			mainUI = (GameObject)Instantiate(mainUIPrefab);
			
			timeText = GameObject.Find("CurrentSongTime").GetComponent<Text>();
			songText = GameObject.Find("Songlist_artist_title").GetComponent<Text>();
			
			popUp = GameObject.Find("SongTag");
			
			detailNextSong = GameObject.Find("Detail_NextSong").GetComponent<Text>();
			detailNextSongOutline = GameObject.Find("Detail_NextSongOutline").GetComponent<Text>();
			
			announcement = GameObject.Find("Announcements").GetComponentInChildren<Text>();
		}
		
		guiInit = true;
	}

    //sam
    private void Update()
    {
        if(MusicManager.instance.musicPlayer != null)
		{
            //timeText.text = MusicManager.instance.musicPlayer.time.ToString() + " / " + MusicManager.instance.selectedSongPrefab.GetComponent<SongScript>().songClip.length.ToString();	
			float tempDuration =  MusicManager.instance.selectedSongPrefab.GetComponent<SongScript>().songClip.length;
			string elapsedTime = string.Format("{0:00}:{1:00}", MusicManager.instance.musicPlayer.time / 60, MusicManager.instance.musicPlayer.time % 60);
			string durationTime = string.Format("{0:00}:{1:00}", tempDuration / 60, tempDuration % 60);
			timeText.text = elapsedTime + " / " + durationTime;
		}
    }

	public void ShowIntroPopup()
	{
		if(popUpHidden == true)
		{

			//print(songText);
			songText.text =
				GameSettings.instance.chosenPlaylist.GetComponent<PlaylistScript>().playlistName +
					" / " + MusicManager.instance.currentSong.GetComponent<SongScript>().songArtist +
					" / " + MusicManager.instance.currentSong.GetComponent<SongScript>().songName;
			
				popUp.GetComponent<Animation>().Play("UI_AnimPopupEnter");

			
			//Announcement
			announcement.transform.parent.gameObject.SetActive(true);
			detailNextSong.gameObject.SetActive(true);
			detailNextSongOutline.gameObject.SetActive(true);
			
			announcement.text = "Get Ready";
			
			//print("1");
			//announcement.GetComponentInParent<Animation>().Play("UI_AnnouncementFadeIn");
			
			string _announcementText = MusicManager.instance.currentSong.GetComponent<SongScript>().songArtist +
				detailNextSongSeparator +
					MusicManager.instance.currentSong.GetComponent<SongScript>().songName;
			
			detailNextSong.text = _announcementText;
			detailNextSongOutline.text = _announcementText;


			popUpHidden = false;
		}
	}

	public void HideIntroPopup()
	{
		if(popUpHidden == false)
		{

			popUp.GetComponent<Animation>().Play("UI_AnimPopupExit");

			//announcement.transform.parent.parent.gameObject 0.76
			//announcement.GetComponentInParent<Animation>().Play("UI_AnnouncementFadeOut");
			
			detailNextSong.gameObject.SetActive(false);
			detailNextSongOutline.gameObject.SetActive(false);
			announcement.transform.parent.gameObject.SetActive(false);

			popUpHidden = true;
		}
	}

    

    public void NewSong()
    {
        if (guiInit)
        {
			ShowIntroPopup();

        }
    }

    public void ShowSongTag()
    {


    }

    //public IEnumerator FadeAnnouncement(float goal)
    //{
    //    while(
    //}


	
	public void StopGUI()
	{
		
	}

	public void DisplayMessage(string messageToDisplay)
	{
		switch (messageToDisplay)
		{
		case "prestart" :
			GetReady.GetComponent<Renderer>().enabled = true;
			GetReady.GetComponent<TextMesh>().text = "Get Ready!";
			break;
		case "start" :
			GetReady.GetComponent<TextMesh>().text = "Go!";
			iTween.FadeTo(GetReady,iTween.Hash ("alpha",0.0f,"time",1.0f,"oncomplete","textFadeOutHandler","oncompleteparams",GetReady,"oncompletetarget",this.gameObject));
			break;
		case "nowinner" :
			WinMessage.GetComponent<Renderer>().enabled = true;
			WinMessage.GetComponent<TextMesh>().text = "No winner!";
			break;
		case "winner" :
			WinMessage.GetComponent<Renderer>().enabled = true;
			WinMessage.GetComponent<TextMesh>().text = "Winner: "+GameManager.instance.winnerName+"!";
			break;
			
		}
	}

	private void textFadeOutHandler(GameObject textGO){
		iTween.FadeTo(textGO,1.0f,0.1f);
		textGO.GetComponent<Renderer>().enabled = false;
	}	


	public void ToggleInstructions(bool showInstructions)
	{
		if (showInstructions == true)
		{
			iTween.MoveTo(InstructionScreen, iTween.Hash("delay",1.0f,"position", new Vector3(0, 0.4f, 1.5f), "time", 1.0f, "islocal", true, "oncomplete", "ToggleInstructions", "oncompleteparams", false, "oncompletetarget", this.gameObject));
		}
		else
		{
			iTween.MoveTo(InstructionScreen, iTween.Hash("delay", 3.0f, "position", new Vector3(0, 0.5f, 1.5f), "time", 1.0f, "islocal", true));
		}
	}


	
	public void ChangeScore(int player,int score)
	{

	}

	public void ChangeGUIColors()
	{
		WinMessage.GetComponent<TextMesh>().color = GraphicsManager.instance.playerColorBasedOnBG;
		GameOverMessage.GetComponent<TextMesh>().color = GraphicsManager.instance.playerColorBasedOnBG;
		RoundIndicator.GetComponent<TextMesh>().color = GraphicsManager.instance.playerColorBasedOnBG;
		GetReady.GetComponent<TextMesh>().color = GraphicsManager.instance.playerColorBasedOnBG;

	}
}
