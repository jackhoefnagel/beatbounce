﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public int player1score = 3;
	public int player2score = 3;
	public int player3score = 3;
	public int player4score = 3;

	private int levelNumber = 1;
	
	private bool startNextLevelTimer  = false;
	private float nextLevelTimer ;
	private float nextLevelTimerInterval  = 5.0f;

	private bool playerHasWon  = false;
	private string tempRoundWinner;
	public string round1Winner;
	public string round2Winner;
	public string winnerName;

	//debug
	public GameObject debugGameSettings;
	private GameObject powerupsObject;
	private bool levelHasPowerups = false;
	public bool debugInput = true;
	
	public float timeScale = 1.0f;

	[SerializeField]
	public AnimationCurve timescaleDipCurve;
	public bool isDoingTimescaleDip = false;
	public float timescaleDipSpeed = 1.0f;

	public static GameManager instance {get; private set;}
	void Awake(){
		instance = this;

		if(GameSettings.instance == null)
		{
			GameObject newGameSettings = (GameObject)Instantiate(debugGameSettings);
			GameSettings.instance = newGameSettings.GetComponent<GameSettings>();
		}
	}

	void Start()
	{
		GameSettings.instance.MakePlaylist();
		GameSettings.instance.StartRound();
		StartGame();
	}

	void Update()
	{
		if (Input.GetKeyUp("0"))
			resetGame();

		Time.timeScale = timeScale;

		if(Input.GetKeyUp("q"))
		{
			DoTimeScaleDip();
		}
	}


	public void DoTimeScaleDip()
	{
		//timeScale = .5f;
		if(!isDoingTimescaleDip)
		{
			StartCoroutine("DoTimeScaleDipCoroutine");
		}
	}

	IEnumerator DoTimeScaleDipCoroutine()
	{
		isDoingTimescaleDip = true;
		float timer = 0.0f;
		while(timer < 1.0f)
		{
			timeScale = timescaleDipCurve.Evaluate(timer);
			timer += Time.fixedDeltaTime * timescaleDipSpeed;
			yield return null;
		}
		timeScale = 1.0f;
		isDoingTimescaleDip = false;
	}

	public void NextRound()
	{
		GameSettings.instance.NextRound();
		resetGame();
		//StartGame();
	}

	public void CheckScore()
	{

		if(player1score > 0 && player2score <= 0 && player3score <= 0 && player4score <= 0)
		{
			//WinMessage.renderer.enabled = true; WinMessage.GetComponent<TextMesh>().text = "Player1 wins!"; 
			tempRoundWinner="player1"; playerHasWon = true;
			//NextRound();
			GameOver();
		}
		else if(player1score <= 0 && player2score > 0  && player3score <= 0 && player4score <= 0)
		{
			//WinMessage.renderer.enabled = true; WinMessage.GetComponent<TextMesh>().text = "Player2 wins!"; 
			tempRoundWinner="player2"; playerHasWon = true;
			//NextRound();
			GameOver();
		}
		else if(player1score <= 0 && player2score <= 0 && player3score >  0 && player4score <= 0)
		{
			//WinMessage.renderer.enabled = true; WinMessage.GetComponent<TextMesh>().text = "Player3 wins!"; 
			tempRoundWinner="player3"; playerHasWon = true;
			//NextRound();		}
			GameOver();
		}
		else if(player1score <= 0 && player2score <= 0 && player3score <= 0 && player4score > 0 )
		{
			//WinMessage.renderer.enabled = true; WinMessage.GetComponent<TextMesh>().text = "Player4 wins!"; 
			tempRoundWinner="player4"; playerHasWon = true;
			//NextRound();		}
			GameOver();
		}
		else if(player1score <= 0 && player2score <= 0 && player3score <= 0 && player4score <= 0)
		{
			Debug.Log("niemand heeft gewonnen?"); playerHasWon = false; tempRoundWinner="none"; NextRound();
		}

	}

	public void StartGame()
	{
		MusicManager.instance.markerIndex = 0;

		ResetScores();

		GraphicsManager.instance.InitGraphics();
		PlayerManager.instance.InitPlayers();
		CameraManager.instance.InitCam();
        MusicManager.instance.InitMusic();
		
		LevelManager.instance.InitLevel();

        GUIManager.instance.InitGUI();
		PowerupManager.instance.InitPowerups();

		CameraManager.instance.camMoveScript.startAnimTime = MusicManager.instance.timeUntilFirstBeat;
		CameraManager.instance.camMoveScript.BeginAnimation();


        //GUIManager.instance.ToggleInstructions(true);
	}

	public void resetGame(){

		StartCoroutine("RestartGame");

	}

	IEnumerator RestartGame()
	{
		MusicManager.instance.markerIndex = 0;
		
		ResetScores();
		
		// hard reset, no transition
		GraphicsManager.instance.StopGraphics();
		PlayerManager.instance.StopPlayers();
		CameraManager.instance.StopCam();
		GUIManager.instance.StopGUI();
		LevelManager.instance.StopLevel();
		MusicManager.instance.StopMusic();
		PowerupManager.instance.StopPowerups();

		yield return 0; // wait for a frame so all cleanups are done!
		
		GraphicsManager.instance.InitGraphics();
		PlayerManager.instance.InitPlayers();
		CameraManager.instance.InitCam();
		GUIManager.instance.InitGUI();
		LevelManager.instance.InitLevel();
		MusicManager.instance.InitMusic();
		
		
		/* TODO: make camera animation based on the time it takes until the first beat comes in?
		 * Might provide a better anticipation of when to start playing!
		*/
		CameraManager.instance.camMoveScript.startAnimTime = MusicManager.instance.timeUntilFirstBeat;
		CameraManager.instance.camMoveScript.BeginAnimation();

	}

	public void GameOver()
	{

		winnerName = tempRoundWinner;

		if(playerHasWon == false)
		{
			winnerName = "none";
			GUIManager.instance.DisplayMessage("nowinner");
			//displayMessage("nowinner");
		}
		else{
			GUIManager.instance.DisplayMessage("winner");
		}



		
		if(levelNumber == 1)
		{
			round1Winner = tempRoundWinner;
		}
		if(levelNumber == 2)
		{
			round2Winner = tempRoundWinner;
			// bonus round?
			// which players are competing?

			if(round1Winner == "none" && round2Winner == "none")
			{
				// no winners both rounds: bonus round
			}
			else if(round1Winner != "none" && round2Winner != "none" && round1Winner == round2Winner)
			{
				// player won both rounds: round1Winner Player wins!
				//GameObject.Find("ScoreCounter").GetComponent(ScoreCounterScript).gameWinner = round1Winner;
				//Application.LoadLevel(1);
			}
			else if(round1Winner != "none" && round2Winner != "none" && round1Winner != round2Winner)
			{
				// different players won the rounds: Bonus round with two players!
			}
			else if(round1Winner != "none" && round2Winner == "none")
			{
				// round1Winner Player wins!   --- Questionable!
				//GameObject.Find("ScoreCounter").GetComponent(ScoreCounterScript).gameWinner = round1Winner;
				//Application.LoadLevel(1);
			}
			else if(round1Winner == "none" && round2Winner != "none")
			{
				// round2Winner Player wins!   --- Questionable!
				//GameObject.Find("ScoreCounter").GetComponent(ScoreCounterScript).gameWinner = round2Winner;
				//Application.LoadLevel(1);
			}
			
		}
		if(levelNumber == 3)
		{
			// final winner!
		}

		StartCoroutine("WaitUntilNextRound");

	}

	IEnumerator WaitUntilNextRound()
	{
		yield return new WaitForSeconds(4.0f);
		NextRound();
	}

	private void ResetScores()
	{

		player1score = GameSettings.instance.playersParticipating[0] ? 3 : 0;
		player2score = GameSettings.instance.playersParticipating[1] ? 3 : 0;
		player3score = GameSettings.instance.playersParticipating[2] ? 3 : 0;
		player4score = GameSettings.instance.playersParticipating[3] ? 3 : 0;


	}

}
