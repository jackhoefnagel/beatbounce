﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GraphicsManager : MonoBehaviour {

	public static GraphicsManager instance {get; private set;}
	void Awake(){
		instance = this;
	}

	public enum SongPhase{None, Intense, Chill, Chorus, Bridge, Solo};
	public SongPhase songPhase; 

	public GameObject GraphicsPrefab;
	public GameObject currentGraphics;

	public GameObject ColorPrefab;
	[HideInInspector] public GameObject currentColor;

	private GameObject backgroundColorObject;
	private GameObject bgParticles1;
	private GameObject bgParticles2;
	
	public string str_levelSegmentsColor = "FFFFFF";
	public string str_levelSegmentsDisappearingColor = "FFFFFF";
	public string str_backgroundColor = "FFFFFF";
	public string str_bgParticlesColor1 = "FFFFFF";
	public string str_bgParticlesColor2 = "FFFFFF";
	
	public Color levelSegmentsColor = Color.white;
	public Color levelSegmentsDisappearingColor = Color.white;
	public Color backgroundColor = Color.black;
	public Color bgParticlesColor1 = Color.white;
	public Color bgParticlesColor2 = Color.white;

	[HideInInspector]
	public GameObject pulseBGGO;
	[HideInInspector]
	public GameObject BGColorGO;
	[HideInInspector]
	public GameObject[] bgParticlesColor1GO;
	[HideInInspector]
	public GameObject[] bgParticlesColor2GO;

	private List<GameObject> levelBeatAnimationObjectsList;
	private GameObject levelBeatAnimationObject;
	private int levelBeatAnimationObjectIndex = 0;
	private Dictionary<GameObject, List<AnimationState>> levelBeatAnimationsDictionary;

	public Color playerColorBasedOnBG = Color.white;

	public bool pulseEnabled = false;
	public float pulseBackgroundAlpha = 0.1f;

	private int songPhaseIndex = 0;

	void Start(){

	}

	void Update()
	{
		//DEBUG
		if(Input.GetKeyUp("o"))
		{
			SwitchColorPhase(Random.Range(0,3));
		}


		if(pulseEnabled)
		{
			if(pulseBackgroundAlpha >= 0)
			{
				pulseBackgroundAlpha -= Time.deltaTime * 0.4f;
			}
			if (pulseBGGO != null)
			{
				pulseBGGO.GetComponent<Renderer>().material.color = new Color(1, 1, 1, pulseBackgroundAlpha);
			}
		}
	}

	public void InitGraphics(){
		currentGraphics = null;
		currentGraphics = (GameObject)Instantiate(GraphicsPrefab);

		currentColor = ColorPrefab;
		ColorScheme newColors = currentColor.GetComponent<ColorScheme>();
		levelSegmentsColor = newColors.levelSegmentsColor;
        //levelSegmentsColor.a = 0.5f; //make level semi-transparent to see BG
		levelSegmentsDisappearingColor = newColors.levelSegmentsDisappearingColor;
		backgroundColor = newColors.backgroundColor;
		bgParticlesColor1 = newColors.backgroundGraphicsColorObject1;
		bgParticlesColor2 = newColors.backgroundGraphicsColorObject2;

		//TODO: these objects are stuck to camera...so issues with camera init order (gamemanager), dunno solution yet
		//pulseBGGO = GameObject.FindGameObjectWithTag("PulseBackgroundObject");
		BGColorGO = GameObject.FindGameObjectWithTag("BackgroundColorObject"); 

		//bgParticlesColor1GO = null;
		//bgParticlesColor2GO = null;

		ColorBGGraphics();

		pulseEnabled = true;

		if (levelSegmentsColor.r < 0.5f &&
		    levelSegmentsColor.g < 0.5f &&
		    levelSegmentsColor.b < 0.5f)
		{
			playerColorBasedOnBG = Color.white;
		}
		else
		{
			playerColorBasedOnBG = new Color(0.05f, 0.05f, 0.05f); // dark grey
		}

		levelBeatAnimationObjectsList = new List<GameObject>();
		GameObject[] beatAnimObjects = GameObject.FindGameObjectsWithTag("LevelBeatAnimationObject");
		levelBeatAnimationObjectsList.AddRange(beatAnimObjects);
		foreach(GameObject beatAnimObject in levelBeatAnimationObjectsList)
		{
			if(beatAnimObject.GetComponent<AnimationObjectScript>() == null)
			{
				beatAnimObject.AddComponent<AnimationObjectScript>();
			}
		}

	}

	public void StopGraphics(){

		Destroy(currentGraphics);

		levelBeatAnimationObjectsList.Clear();
		levelBeatAnimationObjectsList = new List<GameObject>();
		foreach(GameObject beatAnimObject in levelBeatAnimationObjectsList)
		{
			Debug.Log("beatAnimObject: "+beatAnimObject);
		}

		pulseEnabled = false;

		bgParticlesColor1GO = null;
		bgParticlesColor2GO = null;

		pulseBGGO = null;

	}

	public void PulseBackground()
	{
		pulseBackgroundAlpha = 0.5f;
	}

	public void ColorBGGraphics()
	{
		if(BGColorGO != null)
		{	iTween.ColorTo(BGColorGO, backgroundColor, 1.0f); }

		//TODO: Why is this lookup even necessary? keeps on displaying "missing" in graphicsmanager when switching levels
		bgParticlesColor1GO = GameObject.FindGameObjectsWithTag("BGParticles1"); 
		bgParticlesColor2GO = GameObject.FindGameObjectsWithTag("BGParticles2");

		foreach(GameObject bgGO in bgParticlesColor1GO)
		{
			if(bgGO.transform.GetComponent<ParticleSystem>() != null){
				bgGO.GetComponent<ParticleSystem>().GetComponent<Renderer>().material.color = bgParticlesColor1;
				bgGO.GetComponent<ParticleSystem>().GetComponent<Renderer>().material.color = bgParticlesColor2;
			}
			else
			{
				bgGO.GetComponent<Renderer>().material.color = bgParticlesColor1;
				bgGO.GetComponent<Renderer>().material.color = bgParticlesColor2;
			}
			
		}

		foreach(GameObject bgGO2 in bgParticlesColor2GO)
		{
			if(bgGO2.transform.GetComponent<ParticleSystem>() != null){
				bgGO2.GetComponent<ParticleSystem>().GetComponent<Renderer>().material.color = bgParticlesColor1;
				bgGO2.GetComponent<ParticleSystem>().GetComponent<Renderer>().material.color = bgParticlesColor2;
			}
			else
			{
				bgGO2.GetComponent<Renderer>().material.color = bgParticlesColor1;
				bgGO2.GetComponent<Renderer>().material.color = bgParticlesColor2;
			}
			
		}


		GameObject[] bgColorObjects1 = GameObject.FindGameObjectsWithTag("BGGraphicsColorObject1"); 
		GameObject[] bgColorObjects2 = GameObject.FindGameObjectsWithTag("BGGraphicsColorObject2"); 
		GameObject[] bgColorObjects3 = GameObject.FindGameObjectsWithTag("BGGraphicsColorObject3"); 

		if(bgColorObjects1 != null)
		{ foreach(GameObject bgCO in bgColorObjects1) { iTween.ColorTo(bgCO, bgParticlesColor1, 1.0f); }}
		if(bgColorObjects2 != null)
		{ foreach(GameObject bgCO in bgColorObjects2) { iTween.ColorTo(bgCO, bgParticlesColor2, 1.0f); }}
		if(bgColorObjects3 != null)
		{ foreach(GameObject bgCO in bgColorObjects3) { iTween.ColorTo(bgCO, backgroundColor, 1.0f); }}

	}

	public void AnimateGraphicsOnBeat()
	{
		if(levelBeatAnimationObjectsList.Count > 0)
		{
			foreach(GameObject beatAnimObject in levelBeatAnimationObjectsList)
			{
				beatAnimObject.GetComponent<AnimationObjectScript>().PlayNextClip();

				/*
				if(levelBeatAnimationObjectIndex < levelBeatAnimationsDictionary.Length - 1)
				{
					//levelBeatAnimationObject.animation.Play(levelBeatAnimationsList[levelBeatAnimationObjectIndex].name);
					levelBeatAnimationObjectIndex++;
				}
				else
				{
					//levelBeatAnimationObject.animation.Play(levelBeatAnimationsList[levelBeatAnimationObjectIndex].name);
					levelBeatAnimationObjectIndex = 0;
				}
				*/
			}
		}
	}


	public void SwitchColorPhase(int songPhaseIndex)
	{


		backgroundColor =                   new Color(Random.value, Random.value, Random.value);
		levelSegmentsColor =                new Color(Random.value, Random.value, Random.value, 0.5f); // semi-transparent level segments to see BG
		levelSegmentsDisappearingColor =    new Color(Random.value, Random.value, Random.value);
		bgParticlesColor1 =                 new Color(Random.value, Random.value, Random.value);
		bgParticlesColor2 =                 new Color(Random.value, Random.value, Random.value);
		

		if (levelSegmentsColor.r < 0.5f &&
		    levelSegmentsColor.g < 0.5f &&
		    levelSegmentsColor.b < 0.5f)
		{
			playerColorBasedOnBG = Color.white;
		}
		else
		{
			playerColorBasedOnBG = new Color(0.05f, 0.05f, 0.05f); // dark grey
		}

		// actually change the colors
		iTween.ColorTo(BGColorGO, backgroundColor, 1.0f);
		PlayerManager.instance.ChangeAllPlayerColors();
		LevelManager.instance.SwitchLevelColors();
		GUIManager.instance.ChangeGUIColors();
		ColorBGGraphics();
		
		// change message text colors
		/* GUI Manager stuff?
		 * 
		WinMessage.GetComponent<TextMesh>().color = playerColorBasedOnBG;
		GameOverMessage.GetComponent<TextMesh>().color = playerColorBasedOnBG;
		RoundIndicator.GetComponent<TextMesh>().color = playerColorBasedOnBG;
		GetReady.GetComponent<TextMesh>().color = playerColorBasedOnBG;
		*/
	}
}
