﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

	public enum InputMethod {
		CustomJoysticks,
		XboxControllers,
		Keyboard
	}
	public InputMethod inputMethod;

	public static InputManager instance {get; private set;}
	
	void Awake(){
		instance = this;
	}

    void Start()
    {
        switch (inputMethod)
        {
            case InputMethod.CustomJoysticks:
                // use serialinput
                break;
            case InputMethod.XboxControllers:
                // use regular input
                break;
            case InputMethod.Keyboard:
                // use keyboard
                break;
        }

    }
	
}
