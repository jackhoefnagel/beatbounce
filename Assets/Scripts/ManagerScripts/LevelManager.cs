﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {

	public static LevelManager instance {get; private set;}
	
	void Awake(){
		instance = this;
	}

	public GameObject selectedLevelPrefab;
	private GameObject currentLevel;

    //private GameObject levelPlatformMesh;
    //private GameObject levelPlatformColliders;


    //private List<GameObject> levelSegments;
    //private int 		lastSegmentInt; 				
    //private GameObject 	lastSegment; 					
    //private GameObject 	firstSegment;
	public int currSegment = 0;
	
	private int spiralColliderSegmentToActivate = 0;
	private int ringColliderSegmentToActivate = 0;
	private GameObject currentlyActiveColliderSegment;
	
	private int spiralLastSegmentDropped = 0;

	public bool platformSegmentsCanDissolve = true;
	public bool lastToFirst = true;
	
	public string[] levelTypes = {"rings","spiral","tetris"};
	public int levelType = 0;
	
	private Color temp_levelSegmentsColor;
	
	private bool blinkOn = true;
	private GameObject objectToBlink;

    //Sam
    private List<GameObject> currentPlatformMeshes = new List<GameObject>();
    private List<GameObject> currentPlatformColliders = new List<GameObject>();
    public bool reverseDeletion = false;
    private bool destructionFinished = false;

	//Jack...
	public bool endlessLevel = false;
	private Vector3 moveSegmentOffset = new Vector3(0,0,20);

	void Start()
	{

	}

	public void InitLevel(){

        

        destructionFinished = false;
		currentLevel = (GameObject)Instantiate(selectedLevelPrefab);

        foreach (Transform _levelChild in currentLevel.GetComponentsInChildren<Transform>())
        {

            string[] _name = _levelChild.name.Split('_');

            if (_name[0] == "g") // graphics
            {
                _levelChild.tag = "LevelMesh"; // For you Jack :)

				if(!endlessLevel)
				{
                	iTween.MoveTo(_levelChild.gameObject, iTween.Hash("y", 0, "islocal", true));
				}
                iTween.ColorTo(_levelChild.gameObject, GraphicsManager.instance.levelSegmentsColor, 0.5f);

                currentPlatformMeshes.Add(_levelChild.gameObject);
            }
            else if( _name[0] == "c") // colliders
            {

                    _levelChild.tag = "LevelColliders"; // For you Jack :)

                    _levelChild.gameObject.AddComponent<MeshCollider>();

                    Destroy(_levelChild.gameObject.GetComponent<MeshRenderer>());
					
					if(!endlessLevel)
					{
                    	_levelChild.gameObject.SetActive(false);
					}
                    currentPlatformColliders.Add(_levelChild.gameObject);
              
            }
        }

        currSegment = (reverseDeletion) ? 0: currentPlatformColliders.Count-1;

        currentPlatformColliders[currSegment].SetActive(true);
	}

    private void ResetLevel()
    {
        Destroy(currentLevel);

        currentPlatformMeshes = new List<GameObject>();
        currentPlatformColliders = new List<GameObject>();

        
    }

	public void StopLevel(){
		

        ResetLevel();

	}


	public void IndicateDisappearSegment()
	{
         if(!destructionFinished)
		{
			iTween.ColorTo(currentPlatformMeshes[currSegment], GraphicsManager.instance.levelSegmentsDisappearingColor, 0.5f);
		}
	}

	public void DisappearSegment()
	{
		if (!destructionFinished && !endlessLevel)
		{
			iTween.MoveTo(currentPlatformMeshes[currSegment], iTween.Hash("y", -20, "time", 0.5f, "oncomplete", "DestroySegment", "oncompletetarget", this.gameObject)); //possible bug: "oncomplete" not working if platformmesh inactive
		}
		else if(!destructionFinished && endlessLevel)
		{
			iTween.MoveTo(currentPlatformMeshes[currSegment], iTween.Hash("z", -20, "time", 0.5f, "oncomplete", "MoveSegmentToNewPosition", "oncompletetarget", this.gameObject)); //possible bug: "oncomplete" not working if platformmesh inactive
		}
	}

	private void MoveSegmentToNewPosition()
	{

		currentPlatformMeshes[currSegment].transform.position += moveSegmentOffset * (currentPlatformMeshes.Count+1);
		currentPlatformColliders[currSegment].transform.position += moveSegmentOffset * (currentPlatformColliders.Count);

		currSegment = (reverseDeletion) ? currSegment + 1 : currSegment - 1;

		if(!endlessLevel)
		{
			if(!reverseDeletion)
				if(currSegment==0)
					destructionFinished = true;
			else
				if(currSegment==currentPlatformColliders.Count)
					destructionFinished = true;
		}
		else
		{
			if(!reverseDeletion)
			{
				if(currSegment==0)
				{
					currSegment = currentPlatformColliders.Count-1;
				}
			}
			else
			{
				if(currSegment==currentPlatformColliders.Count)
				{
					currSegment = 0;
				}
			}

		}

		currentPlatformColliders[currSegment].SetActive(true);

	}

    private void DestroySegment()
    {
        
        //Destroy current
        currentPlatformMeshes[currSegment].SetActive(false);
        currentPlatformColliders[currSegment].SetActive(false);

        currSegment = (reverseDeletion) ? currSegment + 1 : currSegment - 1;

        currentPlatformColliders[currSegment].SetActive(true);

        if(!reverseDeletion)
            if(currSegment==0)
                destructionFinished = true;
        else
            if(currSegment==currentPlatformColliders.Count)
                destructionFinished = true;

    }

	public Vector3 GetRandomPositionOnSegment()
	{
		//TODO: how to get random position on segment for Tetris? 
        //( test raycasting ???)
        float spawnExtentsX = currentPlatformColliders[currSegment].GetComponent<Renderer>().bounds.extents.x / 2;
        float spawnExtentsZ = currentPlatformColliders[currSegment].GetComponent<Renderer>().bounds.extents.z / 2;

        return new Vector3(Random.Range(-spawnExtentsX, spawnExtentsX), 0.0F, Random.Range(-spawnExtentsZ, spawnExtentsZ)); ;
	}

	public void SwitchLevelColors()
	{
		foreach(GameObject segment in currentPlatformMeshes)
		{
			iTween.ColorTo(segment, GraphicsManager.instance.levelSegmentsColor, 0.5f);
		}
	}
	
}
