﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class MusicManager : MonoBehaviour {

	public float markerLatencyOffset = 0.0f;

	public AudioSource musicPlayer;

	private ArrayList markersArray = new ArrayList();
	public int markerIndex  = 0;
	public bool endlessBeats = false;
	private int lastMarkerTime = 0;

	public float countdownUntilFirstBeat = 0.0f;
	public float timeUntilFirstBeat = 10.0f;

	public GameObject selectedSongPrefab;
	[HideInInspector] public GameObject currentSong;
	private AudioClip songClip;
	private TextAsset songMarkers;

	public int everyNBeats_disappearSegment = 16;
	public int everyNBeats_indicateDisappearSegment = 14;
	public int everyNBeats_dropPowerup = 32;

	private bool playingMusic = false;

	public AudioSource soundfx_Bump;
	public AudioSource soundfx_Explosion;
	public AudioSource soundfx_LoseLife;
	public AudioSource soundfx_Dash;

	private bool latencyTimerRunning = false;
	private float latencyTimer = 0.0f;

	private GameObject beatMarkercontainer;


	public static MusicManager instance {get; private set;}	
	void Awake(){
		instance = this;
		
		soundfx_Bump = transform.Find("Sound").Find("SFX_Bump").GetComponent<AudioSource>();
		soundfx_Explosion = transform.Find("Sound").Find("SFX_Explosion").GetComponent<AudioSource>();
		soundfx_LoseLife = transform.Find("Sound").Find("SFX_LoseLife").GetComponent<AudioSource>();
		soundfx_Dash = transform.Find("Sound").Find("SFX_Dash").GetComponent<AudioSource>();
	}


	
	public void InitMusic(){
		currentSong = (GameObject)Instantiate(selectedSongPrefab);
		songClip = currentSong.GetComponent<SongScript>().songClip;
		songMarkers = currentSong.GetComponent<SongScript>().songXMLMarkers;
		
		musicPlayer = transform.Find("Music").gameObject.GetComponent<AudioSource>();
		
		musicPlayer.clip = songClip;
		loadCuePoints();
		musicPlayer.Play();
		playingMusic = true;


        GUIManager.instance.NewSong();
		//TODO: debug test for beat indicator
		GenerateBeatSheet();
		
	}

	private void GenerateBeatSheet()
	{
		if(beatMarkercontainer != null)
		{
			Destroy(beatMarkercontainer);
		}
			beatMarkercontainer = new GameObject("BeatMarkerContainer");

			for (int i = 0; i < markersArray.Count; i++) {
				float beatmarkerSec = (int)markersArray[i];
				beatmarkerSec /= 44100.0f;
				GameObject beatMarker = (GameObject)Instantiate(Resources.Load("TEST_BeatMarkerObject"));
				beatMarker.transform.position = new Vector3(beatmarkerSec * 4,0,0);
				beatMarker.name = i.ToString();
				if(i % 4 == 0)
					beatMarker.GetComponent<Renderer>().material.color = Color.red;
				beatMarker.transform.parent = beatMarkercontainer.transform;
			}

			beatMarkercontainer.transform.parent = CameraManager.instance.currentCam.transform;
			beatMarkercontainer.transform.localPosition = new Vector3(-0.5f,-2.45f,10);
			beatMarkercontainer.transform.localRotation = Quaternion.identity;


	}
	
	public void StopMusic(){
		Destroy(currentSong);
		songClip = null;
		songMarkers = null;
		
		//musicPlayer = transform.Find("Music").gameObject.audio;
		
		musicPlayer.Stop();
		playingMusic = false;
	}


	void Update()
	{

		//DEBUG Keys
		if (Input.GetKeyDown(KeyCode.M))
		{   
			musicPlayer.mute = !musicPlayer.mute;
		}

		if(Input.GetKeyUp("left") || Input.GetKeyUp("up") || Input.GetKeyUp("down") || Input.GetKeyUp("right"))
		{
			latencyTimerRunning = true;
			latencyTimer = 0.0f;
		}
		if(latencyTimerRunning)
		{
			latencyTimer += Time.deltaTime;
		}



		if(playingMusic == true)
		{
			if (endlessBeats == false)
			{
				if (markersArray != null) // so gameover is called here when theres no more beats!
				{				
					if (markerIndex < (markersArray.Count - 1))
					{
						int marker = (int)markersArray[markerIndex];

						if(markerIndex == 0)
						{
                            GUIManager.instance.NewSong();
							countdownUntilFirstBeat = marker - musicPlayer.timeSamples;
							countdownUntilFirstBeat /= 44100.0f; /* time in seconds, sort of */

							// GUI popup close before first beat
							if(countdownUntilFirstBeat < 3.0f)
							{							
								GUIManager.instance.HideIntroPopup();
							}
						}

						int markerLatencyOffsetInSamples = Mathf.CeilToInt(markerLatencyOffset * 44100.0f);
						//int clampedMarkerLatencyOffset = Mathf.Clamp(markerLatencyOffsetInSamples, marker - lastMarkerTime , 0 ); // clamp to not have too big offset
						if (musicPlayer.timeSamples >= (marker - markerLatencyOffsetInSamples))
						{
							OnBeat();
							lastMarkerTime = marker;
						}
					}
					else
					{
						GameManager.instance.GameOver();
					}
				}
			}
			else
			{
				// midi BPM input should call OnBeat(); here

			}
		}

	}

	public void OnBeat()
	{
		if(latencyTimerRunning == true)
		{
			latencyTimerRunning = false;
		}

		GraphicsManager.instance.PulseBackground();
		GraphicsManager.instance.AnimateGraphicsOnBeat();


		if(GameSettings.instance.playersParticipating[0]) 	PlayerManager.instance.player1Script.MovePlayerOnBeat();
		if(GameSettings.instance.playersParticipating[1])	PlayerManager.instance.player2Script.MovePlayerOnBeat();
		if(GameSettings.instance.playersParticipating[2])	PlayerManager.instance.player3Script.MovePlayerOnBeat();
		if(GameSettings.instance.playersParticipating[3])	PlayerManager.instance.player4Script.MovePlayerOnBeat();

		OnCurrentBeat(markerIndex); //fix?
		
		if (endlessBeats == false)
		{
			// -------------- Round is over when all beats are done. Let levelscript decide? ---------------
			if (markerIndex < (markersArray.Count - 1))
			{
				markerIndex++;
				//RoundIndicator.GetComponent<TextMesh>().text = "Beats left: " + (markersArray.Count - markerIndex);
			}
			else if (markerIndex == (markersArray.Count - 1))
			{
				//RoundIndicator.GetComponent<TextMesh>().text = "No more beats!";
			}
		}
		else
		{
			// just increment markerindex with no end
			markerIndex++;
		}
		
		//TODO: test beat prediction
		// time 
	}

	void NextSong(){
		//What happens here?
	}

	void loadCuePoints(){

		TextAsset textAsset = new TextAsset();
		textAsset = songMarkers;

		XmlDocument doc = new XmlDocument();
		
		doc.LoadXml(textAsset.text);
		
		XmlNodeList mainInfo = doc.GetElementsByTagName("CuePoint");
		
		markersArray = new ArrayList();
		foreach(XmlNode mainInfos in mainInfo)
		{
			XmlNodeList subMainInfos = mainInfos.ChildNodes;
			foreach(XmlNode subNode in subMainInfos)
			{
				if(subNode.Name.ToString() == "Time")
				{
					int xmlTime = int.Parse(subNode.InnerText);
					float secondsMarker = xmlTime / 1000.0f;
					int timeSamplesMarker = Mathf.RoundToInt(secondsMarker * 44100);
					markersArray.Add(timeSamplesMarker);
				}
			}
		}
	
		int firstMarker = (int)markersArray[0];
		timeUntilFirstBeat = firstMarker - musicPlayer.timeSamples;
		timeUntilFirstBeat /= 44100.0f; /* time in seconds, sort of */

		//Debug.Log("timeUntilFirstBeat: "+timeUntilFirstBeat);
		
	}

	public void OnCurrentBeat (int currentBeat) {

		// check which kind of level?
		// define actions per # of beats

		if (currentBeat == 0) { 
			GUIManager.instance.DisplayMessage("start");
		}
		if (currentBeat > 0 && currentBeat % everyNBeats_indicateDisappearSegment == 0) // every 7th beat
		{
			LevelManager.instance.IndicateDisappearSegment();
		}
		if (currentBeat > 0 && currentBeat % everyNBeats_disappearSegment == 0) // every 8th beat
		{
			LevelManager.instance.DisappearSegment();
		}
		
		if (currentBeat > 0 && currentBeat % everyNBeats_dropPowerup == 0) // every 12th beat spawn powerup
		{
            //PowerupManager.instance.SpawnPowerup();
		}






	}
}
