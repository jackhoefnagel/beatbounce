﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {

	//public GameObject player1Prefab;
	//public GameObject player2Prefab;
	//public GameObject player3Prefab;
	//public GameObject player4Prefab;
	public GameObject playerPrefab;

	[HideInInspector]
	public GameObject player1;
	[HideInInspector]
	public GameObject player2;
	[HideInInspector]
	public GameObject player3;
	[HideInInspector]
	public GameObject player4;

	[HideInInspector]
	public PlayerScript player1Script;
	[HideInInspector]
	public PlayerScript player2Script;
	[HideInInspector]
	public PlayerScript player3Script;
	[HideInInspector]
	public PlayerScript player4Script;

	public Material player1Material;
	public Material player2Material;
	public Material player3Material;
	public Material player4Material;

	public float playerResetTime = 4.0f;


	public static PlayerManager instance {get; private set;}
	void Awake(){
		instance = this;


	}

	public void InitPlayers()
	{
		if(GameSettings.instance.playersParticipating[0] == true)
		{
			player1 = (GameObject)Instantiate(playerPrefab,LevelManager.instance.selectedLevelPrefab.transform.Find("P1_spawnLocation").position,Quaternion.identity);
			player1.name = "Player1";
			player1Script = player1.GetComponent<PlayerScript>();
			player1Script.playerColor = player1Material;
			player1Script.InitPlayer(1);
		}

		if(GameSettings.instance.playersParticipating[1] == true)
		{
			player2 = (GameObject)Instantiate(playerPrefab,LevelManager.instance.selectedLevelPrefab.transform.Find("P2_spawnLocation").position,Quaternion.identity);
			player2.name = "Player2";
			player2Script = player2.GetComponent<PlayerScript>();
			player2Script.playerColor = player2Material;
			player2Script.InitPlayer(2);
		}

		if(GameSettings.instance.playersParticipating[2] == true)
		{
			player3 = (GameObject)Instantiate(playerPrefab,LevelManager.instance.selectedLevelPrefab.transform.Find("P3_spawnLocation").position,Quaternion.identity);
			player3.name = "Player3";
			player3Script = player3.GetComponent<PlayerScript>();
			player3Script.playerColor = player3Material;
			player3Script.InitPlayer(3);
		}

		if(GameSettings.instance.playersParticipating[3] == true)
		{
			player4 = (GameObject)Instantiate(playerPrefab,LevelManager.instance.selectedLevelPrefab.transform.Find("P4_spawnLocation").position,Quaternion.identity);
			player4.name = "Player4";
			player4Script = player4.GetComponent<PlayerScript>();
			player4Script.playerColor = player4Material;
			player4Script.InitPlayer(4);
		}
	}

	public void StopPlayers(){

		if(player1 != null)
			Destroy(player1); player1 = null;
		if(player2 != null)
			Destroy(player2); player2 = null;
		if(player3 != null)
			Destroy(player3); player3 = null;
		if(player4 != null)
			Destroy(player4); player4 = null;

		player1Script = player2Script = player3Script = player4Script = null;

	}

	public void ChangeAllPlayerColors(){
		//check if null?
		if(player1 != null)	player1Script.ChangePlayerColors();
		if(player2 != null) player2Script.ChangePlayerColors();
		if(player3 != null) player3Script.ChangePlayerColors();
		if(player4 != null) player4Script.ChangePlayerColors();
	}
}
