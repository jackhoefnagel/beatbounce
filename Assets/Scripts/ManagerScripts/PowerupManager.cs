﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerupManager : MonoBehaviour {


	public GameObject[] selectedPowerups;

	private List<GameObject> powerupsOnStage;

	public static PowerupManager instance {get; private set;}
	void Awake(){
		instance = this;
	}

	void Start(){
		powerupsOnStage = new List<GameObject>();
	}

	void Update()
	{
		if(Input.GetKeyUp("v"))
		{
			RemoveAllPowerups();
		}
	}

	public void InitPowerups()
	{

	}

	public void StopPowerups()
	{
		RemoveAllPowerups();
	}

    private GameObject getRandomPowerup()
    {
        int _random = Random.Range(0, selectedPowerups.Length);

        return selectedPowerups[_random];
    }


	public void SpawnPowerup()
	{
       

		GameObject pickup = (GameObject)Object.Instantiate(getRandomPowerup()); // TODO: A mode where the powerup is not random but they get placed in retrning order.

		pickup.transform.position = LevelManager.instance.GetRandomPositionOnSegment();
		pickup.transform.rotation = Quaternion.Euler(new Vector3(-90, 0, 0)); 

		//TODO: powerup animation before spawn
		//pickup.rigidbody.isKinematic = true;
		//pickup.collider.enabled = false;
		//iTween.PunchScale(pickup, iTween.Hash("amount", new Vector3(2.0f, 2.0f, 2.0f), "time", 3.0f, "oncomplete", "CompletePowerupSpawning", "oncompleteparams", pickup, "oncompletetarget", this.gameObject));

		powerupsOnStage.Add(pickup);

		StartCoroutine("DestroyPickupAfterTime",pickup);

	}

	IEnumerator DestroyPickupAfterTime(GameObject pickup)
	{
		yield return new WaitForSeconds(10.0f);
		if(pickup != null)
		{
			DestroyPickup(pickup);
		}
	}

	public void DestroyPickup(GameObject pickup = null)
	{
		if(pickup != null)
		{
			powerupsOnStage.Remove(pickup);
			Destroy(pickup);
		}
	}
	
	private void CompletePowerupSpawning(GameObject powerupToSpawn)
	{
		powerupToSpawn.GetComponent<Rigidbody>().isKinematic = false;
		powerupToSpawn.GetComponent<Collider>().enabled = true;
	}

	public void RemoveAllPowerups()
	{
		for (int i = 0; i < powerupsOnStage.Count; i++) {
			Destroy(powerupsOnStage[i]);
			//powerupsOnStage.Remove(powerupsOnStage[i]);
		}
		powerupsOnStage = new List<GameObject>();
	}

}
