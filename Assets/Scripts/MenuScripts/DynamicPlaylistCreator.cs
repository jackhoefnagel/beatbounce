﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class DynamicPlaylistCreator : PlaylistScript {

    private SongScript[] allSongs;

    //Playlist Metrics
    //TODO Add arrays to the current genre and mood to create more versatile playlists
    public bool matchGenre;
    public SongInfos.Genres currentGenre; 

    public bool matchBPM;
    public Vector2 bPMRange;

    public bool matchMood;
    public SongInfos.SongMoods currentMood;

	// Use this for initialization
	void Start () {

        CreatePlaylist();
	}
	
	// Update is called once per frame
	public void CreatePlaylist() {

        playlistSongs.Clear();

        //Get all Songs
        allSongs = Resources.FindObjectsOfTypeAll<SongScript>();

        for(int i = 0; i < allSongs.Length; i++){
            //Debug.Log(allSongs[i].songName);

            if (!matchGenre && !matchBPM && !matchMood)
            {
                Debug.Log(allSongs[i].songName);
                playlistSongs.Add(allSongs[i].gameObject);
            }
            else
            {
                if (matchGenre && allSongs[i].genre == currentGenre)
                    playlistSongs.Add(allSongs[i].gameObject);

                if (matchBPM && allSongs[i].bpm > bPMRange.x && allSongs[i].bpm < bPMRange.y)
                    playlistSongs.Add(allSongs[i].gameObject);

                if (matchMood && allSongs[i].mood == currentMood)
                    playlistSongs.Add(allSongs[i].gameObject);
            }
        }
	}
}
