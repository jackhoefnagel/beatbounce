﻿using UnityEngine;
using System.Collections;

public class ExitApplication : ExitEvent
{

    // Update is called once per frame
    public override void Action()
    {
        base.Action();
        print("quit");
        Application.Quit();
    }
}
