﻿using UnityEngine;
using System.Collections;

public class SaveGameMode : ExitEvent
{
    public bool playRandom;
    public bool playOnly;

    public override void Action()
    {
        base.Action();

        GameSettings.instance.playSongsRandom = playRandom;

        GameSettings.instance.playSingleSong = playOnly;

        //Application.LoadLevel(1);

    }
}
