﻿using UnityEngine;
using System.Collections;

public class SavePlaylist : ExitEvent {

    //public bool playRandom;
    //public bool playOnly;

    private MenueOption menueOption;

    public void Start()
    {
        menueOption = this.GetComponent<MenueOption>();
    }

    public override void Action()
    {
        base.Action();

        //menueOption.parentLayer.GetComponent<PlaylistScript>();
        //print(menueOption.parentLayer.GetComponent<PlaylistLayer>());
        GameSettings.instance.chosenPlaylist = menueOption.parentLayer.GetComponent<PlaylistLayer>().linkedPlaylist.gameObject;

        GameSettings.instance.firstSongIndex = menueOption.GetComponent<SongOption>().songID;

            //gameSettings.playSongsRandom = playRandom;

            //gameSettings.playSingleSong = playOnly;
    }
}
