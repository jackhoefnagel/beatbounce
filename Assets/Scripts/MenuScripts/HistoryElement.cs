﻿using UnityEngine;
using System.Collections;

public class HistoryElement : MonoBehaviour {

    public TextMesh textName;
    public GameObject background;

    private float visualTextBorder = 0.2F;
    private int lineLength = 10;

	// Use this for initialization
    void Awake()
    {
        textName = GetComponentInChildren<TextMesh>();
    }

    public void AssignName(string _name, Color _backgroundColor, Color _fontColor)
    {   
        string[] words = _name.Split(" "[0]);   // Split string by char " " 
        string result = "";                     // Prepare result
        string line = "";                       // Temp line string

        // for each all words        
        foreach (string s in words)
        {

            //print(temp);
            if (s == "//")
            {
                line = "";
            }
            else
            {
                // Append current word into line
                string temp = line + " " + s;

                // If line length is bigger than lineLength
                if (temp.Length > lineLength)
                {
                    // Append current line into result
                    result += line + "\n";
                    // Remain word append into new line
                    line = s;
                }
                // Append current word into current line
                else
                {
                    line = temp;
                }
            }
        }

        // Append last line into result      
        result += line;
        textName.text = result.Substring(1, result.Length - 1);
        textName.color = _fontColor;

        background = new GameObject("background");

        background.transform.parent = this.transform;

        background.AddComponent<SpriteRenderer>().sprite = (Sprite)Resources.Load("defaultSprite", typeof(Sprite));
        background.GetComponent<SpriteRenderer>().color = _backgroundColor;

        if (background != null)
        {


            Collider2D _coll = textName.gameObject.AddComponent<BoxCollider2D>();

            background.transform.localScale = new Vector2(_coll.bounds.size.x + visualTextBorder / 2.0F, _coll.bounds.size.y + visualTextBorder) * 100;

            float _xAdjustment = _coll.bounds.size.x / 2.0F;

            background.transform.localPosition = new Vector3(-_xAdjustment,0.0F);

        }
    }

}



//Freaking annoying duuuude