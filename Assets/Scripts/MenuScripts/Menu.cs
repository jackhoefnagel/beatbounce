﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Menu : MonoBehaviour
{
	//public int fadeIncrease;
	public float m_fStartDelayMax;
	public string m_sLevelName1;
	public string m_sLevelName2;
	public string m_sLevelName3;
	public string m_sLevelName4;
	public string m_sLevelDisplayTxt1;
	public string m_sLevelDisplayTxt2;
	public string m_sLevelDisplayTxt3;
	public string m_sLevelDisplayTxt4;
	public PlayerSelect m_oPlayerSelect1;
	public PlayerSelect m_oPlayerSelect2;
	public PlayerSelect m_oPlayerSelect3;
	public PlayerSelect m_oPlayerSelect4;
	public Texture m_oLevelThumbEmpty;
	public Texture m_oLevelThumb1;
	public Texture m_oLevelThumb2;
	public Texture m_oLevelThumb3;
	public Texture m_oLevelThumb4;
	public GameObject m_oLevelThumb3DEmpty;
	public GameObject m_oLevelThumb3D1;
	public GameObject m_oLevelThumb3D2;
	public GameObject m_oLevelThumb3D3;
	public GameObject m_oLevelThumb3D4;
	public Texture m_oLevelBackgroundEmpty;
	public Texture m_oLevelBackgroundZonk;
	public Texture m_oLevelBackground1;
	public Texture m_oLevelBackground2;
	public Texture m_oLevelBackground3;
	public Texture m_oLevelBackground4;
	public TextMesh m_oTextLevelNumber;
	public AudioPlayer m_oAudioPlayer1;
	public AudioPlayer m_oAudioPlayer2;
	public AudioPlayer m_oAudioPlayer3;
	public AudioPlayer m_oAudioPlayer4;
	public AudioPlayer m_oAudioPlayerZonk;
	public Color m_oColorEmpty;
	public Color m_oColorZonk;
	public Color m_oColor1;
	public Color m_oColor2;
	public Color m_oColor3;
	public Color m_oColor4;
	//public AudioClip m_oAudioClipZonk;
	public Renderer m_oRendererLevelThumb;
	public Renderer m_oBackgroundPlane;
	public GameObject m_oSeveralLevelsSelected;
	public float m_fTSelectionLoadDuration = 0.8f;
	public GameObject m_oGameObjectBackgroundCircle;
	//public GameObject[] levels;
	private string[] m_asLevelName;
	private string[] m_asLevelDisplayTxt;
	private PlayerSelect[] m_aoPlayerSelect;
	private AudioPlayer[] m_aoAudioPlayer;
	private Color[] m_aoColor;
	private Texture[] m_aoLevelThumb;
	private GameObject[] m_aoLevelThumb3D;
	private Texture[] m_aoLevelBackground;
	private int m_iNumberOfPlayersMatched;
	private bool m_bMatched;
	private float m_fCurrStartDelay;
	
	void Awake()
	{
		Cursor.visible = false;

		m_asLevelName = new string[] {
			m_sLevelName1,
			m_sLevelName2,
			m_sLevelName3,
			m_sLevelName4,
		};
		m_asLevelDisplayTxt = new string[] {
			m_sLevelDisplayTxt1,
			m_sLevelDisplayTxt2,
			m_sLevelDisplayTxt3,
			m_sLevelDisplayTxt4,
		};
		m_aoPlayerSelect = new PlayerSelect[] {
			m_oPlayerSelect1,
			m_oPlayerSelect2,
			m_oPlayerSelect3,
			m_oPlayerSelect4
		};
		m_aoLevelThumb = new Texture[] {
			m_oLevelThumb1,
			m_oLevelThumb2,
			m_oLevelThumb3,
			m_oLevelThumb4,
		};
		m_aoLevelThumb3D = new GameObject[] {
			m_oLevelThumb3D1,
			m_oLevelThumb3D2,
			m_oLevelThumb3D3,
			m_oLevelThumb3D4,
		};
		m_aoLevelBackground = new Texture[] {
			m_oLevelBackground1,
			m_oLevelBackground2,
			m_oLevelBackground3,
			m_oLevelBackground4,
		};
		m_aoAudioPlayer = new AudioPlayer[] {
			m_oAudioPlayer1,
			m_oAudioPlayer2,
			m_oAudioPlayer3,
			m_oAudioPlayer4,
		};
		m_aoColor = new Color[] {
			m_oColor1,
			m_oColor2,
			m_oColor3,
			m_oColor4,
		};
		foreach( PlayerSelect oPS in m_aoPlayerSelect )
		{
			oPS.Init( this );
		}
	}
	void Start()
	{
	}
	void Update()
	{
		int iSelectedLevel = -1;
		int iNumberOfPlayersMatched = 0;
		int iNumberOfPlayersActive = 0;
		//bool bMatched = true;
		bool bAllActivePlayersReady = true;
		bool bMatchedPrev = m_bMatched;
		m_bMatched = false;
		
		foreach( PlayerSelect oPS in m_aoPlayerSelect )
		{
			int l = oPS.GetSelectedLevel();
			if( l >= 0 )
			{
				++iNumberOfPlayersActive;
				
				if( iSelectedLevel < 0 )
				{
					m_bMatched = true;
					iSelectedLevel = l;
					++iNumberOfPlayersMatched;
				}
				else if( iSelectedLevel != l )
				{
					m_bMatched = false;
					//break;
				}
				else
				{
					m_bMatched = true;
					++iNumberOfPlayersMatched;
				}
				
				if( !oPS.IsPlayerReady() )
				{
					bAllActivePlayersReady = false;
				}
			}
		}
		bool bMatchedChanged = ( bMatchedPrev != m_bMatched );
		bool bNumberOfMatchedPlayerChanged = ( iNumberOfPlayersMatched != m_iNumberOfPlayersMatched );
		bool bNumberOfMatchedPlayerChangedAndIncreased =
			( bNumberOfMatchedPlayerChanged && iNumberOfPlayersMatched > m_iNumberOfPlayersMatched );
		
		if( iNumberOfPlayersActive == 0 && iSelectedLevel < 0 )
		{
			m_oBackgroundPlane.material.SetColor( "_Color", m_oColorEmpty );
			m_oBackgroundPlane.material.SetTexture( "_MainTex", m_oLevelBackgroundEmpty );
		}
		
		
		if( m_bMatched && iSelectedLevel >= 0 )
		{
			m_oTextLevelNumber.text = m_asLevelDisplayTxt[ iSelectedLevel ];
			//iSelectedLevel.ToString();
			
			m_oRendererLevelThumb.material.SetTexture( "_MainTex", m_aoLevelThumb[ iSelectedLevel ] );
			/*for( int i=0; i<m_aoLevelThumb3D.Length; i++ )
				m_aoLevelThumb3D[i].SetActive( i == iSelectedLevel );
			m_oLevelThumb3DEmpty.SetActive( false );*/
			m_oGameObjectBackgroundCircle.SetActive( true );

			m_oBackgroundPlane.material.SetColor( "_Color", m_aoColor[ iSelectedLevel ] );
			m_oBackgroundPlane.material.SetTexture( "_MainTex", m_aoLevelBackground[ iSelectedLevel ] );
			m_oAudioPlayerZonk.FadeOut();
			
			if( bMatchedChanged )
			{
				foreach( PlayerSelect oPS in m_aoPlayerSelect )
				{
					oPS.ResetLoadProgress();
					oPS.EnableLoading();
				}
				//}
			
			//if( bMatchedChanged && bNumberOfMatchedPlayerChangedAndIncreased )
			//{
				AudioTrackFadeInAndAllOthersOut( iSelectedLevel );
				
				if( bAllActivePlayersReady )
					m_fCurrStartDelay = 0.0f;
			}
			
			if( bAllActivePlayersReady )
			{
				m_fCurrStartDelay += Time.deltaTime;
				if( m_fCurrStartDelay >= m_fStartDelayMax && iNumberOfPlayersMatched >= 2 )
				{
					Debug.Log( "Start Level: " + iSelectedLevel );
					Application.LoadLevel( m_asLevelName[iSelectedLevel] );
				}
			}
			
			m_oSeveralLevelsSelected.SetActive( false );
		}
		else
		{
			m_oTextLevelNumber.text = "-";
			m_oRendererLevelThumb.material.SetTexture( "_MainTex", m_oLevelThumbEmpty );
			m_oGameObjectBackgroundCircle.SetActive( false );
			//m_oLevelThumb3DEmpty.SetActive( true );
			//foreach( GameObject oLT in m_aoLevelThumb3D )
				//oLT.SetActive( false );
			
			if( bMatchedChanged )
			{
				if( iNumberOfPlayersActive >= 2 )
				{
					m_oBackgroundPlane.material.SetColor( "_Color", m_oColorZonk ); //red );
					m_oBackgroundPlane.material.SetTexture( "_MainTex", m_oLevelBackgroundZonk );
					//iTween.ColorTo( m_oBackgroundPlane.gameObject, iTween.Hash( "color", Color.red, "time", 0.3f ) ); 
					//audio.PlayOneShot( m_oAudioClipZonk );
					m_oAudioPlayerZonk.FadeIn();
					
					m_oSeveralLevelsSelected.SetActive( true );

					foreach( PlayerSelect oPS in m_aoPlayerSelect )
					{
						oPS.DeselectLevel();
						oPS.DisableLoading();
					}
				}
			}

			// tmp
			if( iNumberOfPlayersActive == 0 )
			{
				m_oAudioPlayerZonk.FadeOut();
				m_oSeveralLevelsSelected.SetActive( false );
			}

			AudioTrackFadeAllOut();
		}
		
		m_iNumberOfPlayersMatched = iNumberOfPlayersMatched;
		/*
		m_oBackgroundPlane.material.SetColor( "_Color", new Color(
				0.0f,
				0.0f,
				0.0f
			) );
		*/

		if( Input.GetKeyDown( KeyCode.Escape ) )
		{
			Application.Quit();
		}
	}
	public float GetLoadDuration()
	{
		return m_fTSelectionLoadDuration;
	}
	public bool DoesLevelSelectionMatch()
	{
		return m_bMatched;
	}
	private void AudioTrackFadeInAndAllOthersOut( int iTrack )
	{
		m_aoAudioPlayer[iTrack].FadeIn();
		for( int i=0; i<m_aoAudioPlayer.Length; ++i )
		{
			if( i != iTrack )
				m_aoAudioPlayer[i].FadeOut();
		}
	}
	private void AudioTrackFadeAllOut()
	{
		foreach( AudioPlayer oAP in m_aoAudioPlayer )
			oAP.FadeOut();
	}
	
	//public void AudioTrackFadeIn(AudioSource _sourceIn)
	//{
	//    _sourceIn.volume += fadeIncrease;
	//}
	//
	//public void AudioTrackFadeOut(AudioSource _sourceOut)
	//{
	//    _sourceOut.volume -= fadeIncrease;
	//}
}
