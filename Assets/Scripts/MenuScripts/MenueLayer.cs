﻿using UnityEngine;
using System.Collections;

public class MenueLayer : MonoBehaviour {

    public MenueOption[] allButtons;

    public MenueLayer previousLayer;
    public MenueOption previousLayerButton;
    //public MenueLayer nextLayer;

    public Color layerColor;
    public Color contrastColor;
    public Color fontColor = Color.white;

    public float longestButton;
    private float spaceInbetweenY = 0.1F;

    protected MenueManager menueManager;

    public virtual void Awake()
    {
        menueManager = GameObject.Find("MenueManager").GetComponent<MenueManager>();
    }

    public void Start()
    {
        for(int i = 0; i < allButtons.Length; i++)
        {

            if (!allButtons[i].buttonInitiated)
                allButtons[i].Init();

            //Give out the adajcient buttons
            if (i == 0)
            {
                allButtons[i].previousOption = allButtons[allButtons.Length - 1];
            }
            else
            {
                allButtons[i].previousOption = allButtons[i - 1];
            }

            if (i == allButtons.Length - 1)
            {
                allButtons[i].nextOption = allButtons[0];
            }
            else
            {
                allButtons[i].nextOption = allButtons[i + 1];
            }

            //Sort the buttons
            //Vector3 newPosition = new Vector3(0.0F, i * (allButtons[i].transform.localScale.y + spaceInbetweenY));
            //allButtons[i].transform.localPosition = newPosition;

            float currentButtonLenght = allButtons[i].background.transform.localScale.x/100;
            
            if (currentButtonLenght > longestButton)
                longestButton = currentButtonLenght/2;

            //this.transform.localScale = Vector3.one;
        }

        SortButtons();

        ToggleLayer(false);
    }

    public void SortButtons()
    {
        float oldPosition = 0.0F;

        for (int i = 0; i < allButtons.Length; i++)
        {

            Vector3 newPosition = new Vector3(0.0F, -i * ((allButtons[i].background.transform.localScale.y / 2 / 100) + spaceInbetweenY));

            if (i != 0)
            {
                newPosition = new Vector3(0.0F, oldPosition - ((allButtons[i].background.transform.lossyScale.y / 2 / 100) + spaceInbetweenY));

            }


            allButtons[i].transform.localPosition = newPosition;
            oldPosition = newPosition.y - (allButtons[i].background.transform.lossyScale.y / 2 / 100);
        }
    }

    public void ToggleLayer(bool _state)
    {
        //
        foreach (MenueOption _button in allButtons)
        {
            _button.gameObject.SetActive(_state);
        }
    }
}
