﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenueManager : MonoBehaviour
{
    public MenueOption currentButton;

    private float inputDelay = 0.2F; // higher for the actual controller controlled inputs
    private bool inputLocked = false;

    private float timeSinceLastInput = 0.0F;
    private float maxTimeOutForIdleMode = 10.0F;

    private float camTransitionTime = 0.65F;

    public MenueLayer[] allLayers;

    public GameObject historyPrefab;
    public List<GameObject> historyElements;

    public float unselectedButtonScale = 0.55F;
    private float offsetLayer = 0.4F;

    private bool _running = false;

    public MenueLayer gamemodeSelection;

    public GameObject idlePicture;

    public GameObject contrastElement;

    public Color targetColor;
    public Color contrastColor;

    public void InitManager()
    {
        float oldPosition = 0.0F;

        for (int i = 0; i < allLayers.Length; i++)
        {

            //Layer Positions
            Vector3 newPosition = new Vector3( oldPosition + allLayers[i].longestButton+ 0.18F, 0.0F);
            allLayers[i].transform.localPosition = newPosition;
            oldPosition = newPosition.x;
            

            //foreach (MenueOption _option in allLayers[i].allButtons)
            //{
            //    //_option.transform.localScale = Vector3.one * unselectedButtonScale;
            //}


        }

        allLayers[0].ToggleLayer(true);        

        ChangeOption(allLayers[0].allButtons[0]);

        targetColor = allLayers[0].layerColor;

        _running = true;
    }


    public void Update()
    {
        if (!_running)
            InitManager();

        //timeSinceLastInput += Time.deltaTime;

        //if (timeSinceLastInput > maxTimeOutForIdleMode)
        //{
        //    idlePicture.SetActive(true);
        //}

        Camera.main.backgroundColor = Color.Lerp(Camera.main.backgroundColor, targetColor, 0.080F);
        contrastElement.GetComponent<Renderer>().material.SetColor("_Color", Color.Lerp(contrastElement.GetComponent<Renderer>().material.color, contrastColor, 0.080F));

        if (!inputLocked)
        {
            for (int i = 0; i < 4; i++)
            {

                int _direction = -1;
                Vector2 _directionVector = SerialInput.GetDir(i+1);

                if ((int)_directionVector.x == 1)
                {
                    _direction = 3;
                }

                if ((int)_directionVector.x == -1)
                {
                    _direction = 2;
                }

                if ((int)_directionVector.y == 1)
                {
                    _direction = 1;
                }

                if ((int)_directionVector.y == -1)
                {
                    _direction = 0;
                }

                if (_direction != -1)
                {
                    StopCoroutine("UnlockInput");
                    StartCoroutine(UnlockInput());

                    currentButton.GetInput(i, _direction);
                    inputLocked = true;
                   

                   
                }
            }
        }
    }

    public void ChangeOption(MenueOption _newOption)
    {
        currentButton.transform.localScale = Vector3.one * unselectedButtonScale;

        if (currentButton.GetComponent<AudioSource>())
        {
            currentButton.GetComponent<AudioSource>().Stop();
        }

        currentButton = _newOption;

        currentButton.transform.localScale = Vector3.one;

        currentButton.parentLayer.SortButtons();

        iTween.MoveTo(Camera.main.gameObject, iTween.Hash("position", new Vector3(currentButton.transform.position.x +4.0F, currentButton.transform.position.y, Camera.main.transform.position.z),
                            "time", camTransitionTime));
        //Camera.main.transform.position = new Vector3(currentButton.transform.position.x, currentButton.transform.position.y, Camera.main.transform.position.z);

        if (currentButton.GetComponent<AudioSource>())
        {
            currentButton.GetComponent<AudioSource>().Play();
        }
    }

    public void AddHistory(MenueOption _fixedOption)
    {
        GameObject _newHistory = (GameObject)Instantiate(historyPrefab);
        _newHistory.GetComponent<HistoryElement>().AssignName(_fixedOption.name, _fixedOption.parentLayer.layerColor, _fixedOption.parentLayer.fontColor);

        //if(historyElements.Count > 0)
        //    _newHistory.transform.localPosition = new Vector3(currentButton.transform.localPosition.x, historyElements[historyElements.Count-1].transform.position.y - _newHistory.GetComponent<HistoryElement>().background.transform.localScale.y/100);
        //else
            //for (int i = 0; i < historyElements.Count - 1; i++)
            //{

            //}

        //_newHistory.transform.localPosition = currentButton.transform.localPosition;// historyElements[i].transform.position.x
        

        historyElements.Add(_newHistory);

        //SortHistory();
    }

    public void RemoveHistory()
    {
        Destroy(historyElements[historyElements.Count - 1]);

        historyElements.RemoveAt(historyElements.Count - 1);

        //SortHistory();
    }

    public void ChangeLayer(MenueLayer _newLayer, MenueLayer _currentLayer, MenueOption _oldButton)
    {
        print("gogo" + _currentLayer);
        //if (_newLayer.previousLayer != currentButton.nextLayer)

        _currentLayer.ToggleLayer(false);

        _newLayer.ToggleLayer(true);

        //if(_oldButton == null)
        //    _newLayer.previousLayerButton = currentButton;
        

        //Layer Positions
        Vector3 newPosition = new Vector3(currentButton.transform.localPosition.x + currentButton.background.transform.lossyScale.x/100 + offsetLayer, 0.0F);
        _newLayer.transform.localPosition = newPosition;

        //print(_newLayer.previousLayer.previousLayerButton.name _newLayer.previousLayerButton.name);

        if (_oldButton != null)
        {
            
            ChangeOption(_oldButton);
            RemoveHistory();

        }
        else
        {
            _newLayer.previousLayer = _currentLayer;

            _newLayer.previousLayerButton = currentButton;

            AddHistory(currentButton);
            ChangeOption(_newLayer.allButtons[0]);

        }
        if(historyElements.Count > 0)
            SortHistory();

        targetColor = _newLayer.layerColor;
        contrastColor = _newLayer.contrastColor;
    }

    public IEnumerator UnlockInput()
    {
        yield return new WaitForSeconds(inputDelay);

        inputLocked = false;
    }

    public void SortHistory()
    {

        float oldPositionX = currentButton.transform.position.x - 0.23F;
        float oldPositionY = currentButton.transform.localPosition.y; //-historyElements[historyElements.Count - 1].GetComponent<HistoryElement>().background.transform.lossyScale.y / 137.5F;

        for (int i = historyElements.Count - 1; i > -1; i--)
        {
        //for(int i = 0; i < historyElements.Count; i++){
            print(i + " haha");
            Vector3 newPosition = currentButton.transform.localPosition;//new Vector3(historyElements[i].transform.position.x, i * ((historyElements[i].GetComponent<HistoryElement>().background.transform.localScale.y / 2 / 100) + 0.2F));

            print(historyElements[i].GetComponent<HistoryElement>().background.GetComponent<SpriteRenderer>().sprite.bounds);

            float positionChangeY = historyElements[i].GetComponent<HistoryElement>().background.transform.lossyScale.y / 100;

            newPosition = new Vector3(oldPositionX, oldPositionY + positionChangeY/2, 0.0F);

            historyElements[i].transform.localPosition = newPosition;
            oldPositionY = newPosition.y + positionChangeY/2;
        }
    }
}
