﻿using UnityEngine;
using System.Collections;

public class MenueOption : MonoBehaviour {

    public MenueOption previousOption;
    public MenueOption nextOption;

    public MenueLayer specialPreviousLevel;
    public MenueLayer nextLayer;

    public MenueLayer parentLayer;
    protected MenueManager menueManager;

    public TextMesh textElement;
    public GameObject background;

    public float visualTextBorder = 0.2F;

    public float unselectedButtonScale = 0.55F;

    public ExitEvent exitEvent;
    private float characterSize = 0.05F;

    public bool buttonInitiated = false;

	// Use this for initialization
	public virtual void Init () {

        parentLayer = this.GetComponentInParent<MenueLayer>();
        menueManager = GameObject.Find("MenueManager").GetComponent<MenueManager>();

        textElement = GetComponentInChildren<TextMesh>();
        background = new GameObject("background");

        if (nextLayer != null && nextLayer.GetComponent<PlaylistLayer>() != null)
            name = nextLayer.GetComponent<PlaylistLayer>().linkedPlaylist.playlistName;

        textElement.text = name;
        textElement.characterSize = characterSize;
        textElement.color = parentLayer.fontColor;

        background.transform.parent = this.transform;

        background.AddComponent<SpriteRenderer>().sprite = (Sprite)Resources.Load("defaultSprite", typeof(Sprite));

        if (background != null)
        {
            //print(textElement);

            Collider2D _coll = textElement.gameObject.AddComponent<BoxCollider2D>();

            background.transform.localScale = new Vector2(_coll.bounds.size.x + visualTextBorder / 2.0F, _coll.bounds.size.y + visualTextBorder) * 100;

            float _xAdjustment = _coll.bounds.size.x / 2.0F;

            background.GetComponent<SpriteRenderer>().color = parentLayer.layerColor;
            background.transform.localPosition = new Vector3(_xAdjustment, 0.0F);
        }

        exitEvent = GetComponent<ExitEvent>();

        this.transform.localScale = Vector3.one * unselectedButtonScale;

        buttonInitiated = true;
        //print("googl");
        //Shrink it to "deactivate"
        //this.transform.localScale = Vector3.one * 0.2F;
	}
	
	// 0,  optionBack, 1, option forward, 2 layerback, 3, layerforwars
    public void GetInput(int _playerID, int _direction)
    {

        //print(_direction);

        switch (_direction)
        {
            case 0:
                //print("optionBack");
                OptionUp(_playerID);
                //ChangeButton(previousOption);
                break;

            case 1:
                //print("optionforward");
                OptionDown(_playerID);
                //ChangeButton(nextOption);
                break;

            case 2:
                LayerBack(_playerID);
                break;

            case  3:
                LayerForward(_playerID);
                break;
                
            default:
                Debug.Log("Oh that is strange, why is the input wrong");
                break;

        }
	}

    public virtual void OptionUp(int _id)
    {
        menueManager.ChangeOption(previousOption);
    }

    public virtual void OptionDown(int _id)
    {
        print("Option up");
        menueManager.ChangeOption(nextOption);
    }

    public virtual void LayerForward(int _id)
    {
        print("layerforward");
        if (nextLayer != null)
        {
            if (exitEvent != null) exitEvent.Action();
            menueManager.ChangeLayer(nextLayer, parentLayer, null);
        }
    }

    public virtual void LayerBack(int _id)
    {
        print("layerback");
        if (parentLayer.previousLayer != null || specialPreviousLevel != null)
        {
            if (exitEvent != null) exitEvent.Action();

            if (specialPreviousLevel != null)
                menueManager.ChangeLayer(specialPreviousLevel, parentLayer, specialPreviousLevel.allButtons[0]);
            else
                menueManager.ChangeLayer(parentLayer.previousLayer, parentLayer, parentLayer.previousLayerButton);
        }
    }
}
