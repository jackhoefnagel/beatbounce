﻿using UnityEngine;
using System.Collections;

public class MenuePlayerReady : MenueOption
{
    public int minimumPlayerCount = 2;
    public int playersReady = 0;

    //public bool[] playerReadyness;//= new bool[4];

    public GameObject[] playerIcons;
    //public Vector3 playerReady
    public float unReadyPosition;
    public float readyPosition;

    //Timers
    public float currentWaitingTime = 0.0F;
    public float maxWaitingTime = 5.0F;

    public float currentCountdownTime = 0.0F;
    public float maxCountdownTime = 3.0F;

    public TextMesh displayTimer;

    public void Awake()
    {
        //playerReadyness = new bool[4];

        foreach (GameObject _object in playerIcons)
        {
            _object.transform.localPosition = new Vector3(unReadyPosition, _object.transform.localPosition.y);
        }

        StopCountdown();
    }

    private void ResetValues()
    {
        for (int i = 0; i < 4; i ++)
        {
            UnreadyPlayer(i);
        }
    }

    public override void LayerForward(int _id)
    {
        //base.LayerForward(_id);

        if (GameSettings.instance.playersParticipating[_id] == false)
            ReadyPlayer(_id);
    }

    public override void LayerBack(int _id)
    {
        //base.LayerBack(_id);

        if (GameSettings.instance.playersParticipating[_id] == true)
        {
            UnreadyPlayer(_id);
            }
        else
        {
            if (playersReady == 0)
            {
                menueManager.ChangeLayer(parentLayer.previousLayer, parentLayer, parentLayer.previousLayerButton);
            }
        }
    }

    public void ReadyPlayer(int _playerId)
    {
        Debug.Log("PLAYER READY");
        GameSettings.instance.playersParticipating[_playerId] = true;
        //playerReadyness[_playerId] = true;
        playersReady++;
        playerIcons[_playerId].transform.localPosition = new Vector3(readyPosition, playerIcons[_playerId].transform.localPosition.y); ; 

        if (playersReady >= minimumPlayerCount)
        {
            StopCountdown();
            StartCoroutine(GameCountdown());
        }

    }

    public void UnreadyPlayer(int _playerId2)
    {
        GameSettings.instance.playersParticipating[_playerId2] = false;
        //playerReadyness[_playerId2] = false;
        playersReady--;

        playerIcons[_playerId2].transform.localPosition = new Vector3(unReadyPosition, playerIcons[_playerId2].transform.localPosition.y);

        StopCountdown();
        if (playersReady >= minimumPlayerCount)
        {
            print("ne");
            StartCoroutine(GameCountdown());
        }
        
    }

    private void StopCountdown()
    {
        StopAllCoroutines();

        currentWaitingTime = 0.0F;
        displayTimer.text = "Press Right to \n join the game";
    }

    private IEnumerator GameCountdown()
    {
        //yield return new WaitForSeconds(5);
        while (currentWaitingTime < maxWaitingTime)
        {
            currentWaitingTime += Time.deltaTime;
            displayTimer.text = "Bounce in " + Mathf.CeilToInt(maxWaitingTime - currentWaitingTime).ToString();

            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(StartGame());
    }

    private IEnumerator StartGame()
    {
        yield return new WaitForSeconds(1.0F);

        //ResetValues();

        Application.LoadLevel(1);
    }
}

