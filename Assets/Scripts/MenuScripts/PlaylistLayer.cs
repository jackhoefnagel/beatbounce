﻿using UnityEngine;
using System.Collections;

public class PlaylistLayer : MenueLayer
{
    public PlaylistScript linkedPlaylist;
    public GameObject songEntryPrefab;

    //public PlaylistScript linkedPlayList;

    private string songName;
    private string artistName;
    //private s

    public override void Awake()
    {
        //base.Awake();

        //print(GameObject.Find("GamemodeSelection"));
        MenueLayer _hackLayer = GameObject.Find("GamemodeSelection").GetComponent<MenueLayer>();

        //linkedPlaylist = this.GetComponentInChildren<PlaylistScript>();

        allButtons = new MenueOption[linkedPlaylist.playlistSongs.Count];

        for (int i = 0; i < linkedPlaylist.playlistSongs.Count; i++)
        {
            GameObject _newEntry = (GameObject)Instantiate(songEntryPrefab);

            _newEntry.transform.parent = this.transform;            

            allButtons[i] = _newEntry.GetComponent<MenueOption>();

            //allButtons[i].Init();

            songName = linkedPlaylist.playlistSongs[i].GetComponent<SongScript>().songName;
            artistName = linkedPlaylist.playlistSongs[i].GetComponent<SongScript>().songArtist;

            //allButtons[i].textElement.text = artistName.TrimEnd() + " - " + songName.TrimEnd();
            allButtons[i].name = artistName.TrimEnd() +" - " + songName.TrimEnd();

            allButtons[i].GetComponent<SongOption>().songID = i;

            AudioSource _audioSource = allButtons[i].gameObject.AddComponent<AudioSource>();

            _audioSource.clip = linkedPlaylist.playlistSongs[i].GetComponent<SongScript>().songClip;
            _audioSource.playOnAwake = false;
            _audioSource.Stop();


            //print(menueManager.gamemodeSelection);
            allButtons[i].nextLayer = _hackLayer;
            //print(allButtons[i].nextLayer.name + " __ " + menueManager.gameModeSelector);

            allButtons[i].Init();
        }

        SortButtons();
    }
}
