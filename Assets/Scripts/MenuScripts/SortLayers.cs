﻿using UnityEngine;
using System.Collections;

public class SortLayers : MonoBehaviour
{

    public string currentLayer;
    public int orderInLayer;

    // Use this for initialization
    void Start()
    {
        GetComponent<Renderer>().sortingLayerName = currentLayer;

        GetComponent<Renderer>().sortingOrder = orderInLayer;
    }
}
