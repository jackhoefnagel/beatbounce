﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimationObjectScript : MonoBehaviour {

	private List<AnimationState> animStates;
	private int animStateIndex = 0;

	void Awake () {
		animStates = new List<AnimationState>();
		foreach(AnimationState animState in GetComponent<Animation>())
		{
			animStates.Add(animState);
		}
	}



	public void PlayNextClip()
	{
		GetComponent<Animation>().Play(animStates[animStateIndex].name);

		if(animStateIndex < animStates.Count - 1)
		{
			animStateIndex++;
		}
		else
		{
			animStateIndex = 0;
		}
	}
}
