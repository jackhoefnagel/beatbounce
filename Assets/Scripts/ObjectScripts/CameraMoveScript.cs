﻿using UnityEngine;
using System.Collections.Generic;

public class CameraMoveScript : MonoBehaviour {
	
	public float damping = 2.0f;
	
	public Vector3 offsetRotation = Vector3.zero;
	public Vector3 offsetPosition = Vector3.zero;
	
	private Vector3 startPosition;


	public GameObject player1;
	public GameObject player2;
	public GameObject player3;
	public GameObject player4;
	
	private List<GameObject> playersArray;
	
	public bool beginAnimationPlaying;
	
	public float startAnimTime;
	
	public GameObject centerObject;
	
	public string levelName;
	
	public bool doScreenShake = false;
	public float screenShakeMagnitude = 2.0f;
	private float screenShakeTime = 0.25f;
	private float screenShakeTimer = 0.1f;

	private float distance = 0.0f;

	public bool sceneInitialized = false;

	void Awake()
	{
		GetPlayers();
		
		startPosition = transform.position;
		
		transform.position = startPosition + offsetPosition;
		
		
		//BeginAnimation();
	}

	public void InitCam()
	{
		sceneInitialized = true;
	}

	public void StopCam()
	{
		sceneInitialized = false;
	}

	
	void Update () {
		if(sceneInitialized)
		{
			if(!beginAnimationPlaying)
			{
				Vector3 playersCenterPosition = new Vector3();
				int amountOfPlayersActive = 0;

				for (int i = 0; i < GameSettings.instance.playersParticipating.Length; i++) {
					if(GameSettings.instance.playersParticipating[i] == true)
					{
						playersCenterPosition += playersArray[i].transform.position;
						amountOfPlayersActive++;
					}
				}

				playersCenterPosition += offsetRotation;
				playersCenterPosition /= amountOfPlayersActive+1;


				float tempYOffset = 0;
				for (int i = 0; i < GameSettings.instance.playersParticipating.Length; i++) {
					if(GameSettings.instance.playersParticipating[i] == true)
					{
						float tempDistance = Vector3.Distance(playersArray[i].transform.position, playersCenterPosition);
						if (tempDistance > tempYOffset)
						{
							tempYOffset = tempDistance;
						}
					}
				}

				
				
				offsetPosition = new Vector3(0,tempYOffset,0);


				Vector3 look = playersCenterPosition - transform.position;
				
				Quaternion rotate = Quaternion.LookRotation(look,Vector3.forward);
				transform.rotation = Quaternion.Slerp(transform.rotation, rotate, Time.deltaTime * damping);
				
				Vector3 camPosition;			
				if (!doScreenShake)
				{
					camPosition = startPosition + new Vector3(playersCenterPosition.x, 0, playersCenterPosition.z) + offsetPosition;
				}
				else
				{
					camPosition = startPosition + new Vector3(playersCenterPosition.x, 0, playersCenterPosition.z) + offsetPosition + new Vector3(Random.Range(-screenShakeMagnitude, screenShakeMagnitude), Random.Range(-screenShakeMagnitude, screenShakeMagnitude), Random.Range(-screenShakeMagnitude, screenShakeMagnitude));
					if(screenShakeTimer >= 0)
					{
						screenShakeTimer -= Time.deltaTime;
					}
					else
					{
						doScreenShake = false;
					}
				}


				/* Cam Debug Test mouse movement
				if (Input.GetAxis("Mouse ScrollWheel") > 0)
					Camera.main.fieldOfView++;
				if (Input.GetAxis("Mouse ScrollWheel") < 0)
					Camera.main.fieldOfView--;
				if(Input.GetAxis("CamDistance") > 0)
					distance++;
				if(Input.GetAxis("CamDistance") < 0)
					distance--;

				Vector3 mouseMoveOffset = new Vector3((Input.mousePosition.x - Screen.width/2)/2,distance,(Input.mousePosition.y - Screen.height/2)/2); 
				camPosition += mouseMoveOffset;
				*/

				iTween.MoveUpdate(gameObject, camPosition, 1.0f);
			}
		}
	}

	public void GetPlayers()
	{
		playersArray = new List<GameObject>();
		playersArray.Add(PlayerManager.instance.player1);
		playersArray.Add(PlayerManager.instance.player2);
		playersArray.Add(PlayerManager.instance.player3);
		playersArray.Add(PlayerManager.instance.player4);
	}
	
	public void BeginAnimation()
	{
		beginAnimationPlaying = true;
		switch (levelName)
		{
		case "Savant" :
			iTween.MoveFrom(gameObject, iTween.Hash("position", new Vector3(0.0f, -180f, -140), "easetype", "easeInOutQuad", "time", startAnimTime));
			iTween.RotateFrom(gameObject, iTween.Hash("rotation", new Vector3(-45f, 0.0f, 0.0f), "easetype", "easeInOutQuad", "time", startAnimTime, "oncomplete", "BeginAnimationFinished"));
			break;
		default :
			iTween.MoveFrom(gameObject, iTween.Hash("position", new Vector3(0.0f, 12.42524f, -52.53772f), "easetype", "easeInOutQuad", "time", startAnimTime));
			iTween.RotateFrom(gameObject, iTween.Hash("rotation", new Vector3(16.96553f, 0.0f, 0.0f), "easetype", "easeInOutQuad", "time", startAnimTime, "oncomplete", "BeginAnimationFinished"));
			break;
		}
	}
	
	public void BeginAnimationFinished()
	{
		beginAnimationPlaying = false;
	}

	public void DoScreenShake()
	{
		screenShakeTimer = screenShakeTime;
		doScreenShake = true;
	}
}