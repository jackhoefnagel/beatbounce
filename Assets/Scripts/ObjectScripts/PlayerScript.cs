﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerScript : MonoBehaviour {

	private GameManager mainGameScript;
	
	private GameObject P1_spawnLocation;
	private GameObject P2_spawnLocation;
	private GameObject P3_spawnLocation;
	private GameObject P4_spawnLocation;

	public bool playerCanMove = true;
	
	private float boostControllerPressTime = 0.1f;
	private float boostTimer; 
	private bool boostTimerRunning = false;
	public bool boostEnabled = false;
	public bool boostTimerEnabled = false;
	
	public float pulseSpeed = 750.0f;
	public float normalPulseSpeed = 750.0f;
	public float modifiablePulseSpeed = 750.0f;
	public bool allowBoost = true;

	private float rememberInputTimer = 0.0f;
	private float rememberInputTime = 0.15f;
	private Vector2 rememberedInput = Vector2.zero;
	
	private bool runPowerupTimer = false;
	private float powerupTimer;
	private float powerupDuration = 10.0f;
	
	private int amountOfGrowPowerups = 0;
	private int amountOfFasterPowerups = 0;
	private bool playerCanPickupPowerups = true;

	private GameObject[] arrows;
	private GameObject arrow; //new
	private float arrowDistance = 0.25f; // new
	
	private Vector3 startScale;
	private Color startColor;
	
	private bool beatlessControls = false;
	private bool canBounceOthers = true;
	
	//New Player Graphics
	private GameObject lifes3;
	private GameObject lifes2;
	public GameObject currentLifeSign;
	private GameObject playerBaseRing;
	private Color playerColorBasedOnBG;
	private GameObject playerFlash_Alpha;
	
	public float animTimeLoseLife;
	
	private GameObject lifeCounter3;
	private GameObject lifeCounter2;
	private GameObject lifeCounter1;
	private GameObject lifeCounter0;
	
	public Material black;
	public Material white;
	public Material playerColor;
	
	public Light playerLight;
	private Projector projectorLight;
	
	private Rigidbody playerRigidbody;
	
	private int currentPlayerIndex;
	private bool playerInitialised = false;


    public GameObject playerBody;
	private GameObject animContainer;
	private Animation playerBodyAnim;
    public ParticleSystem driftEffect;
    public ParticleSystem moveEffect;
    public GameObject hitEffect;

	private float blinkTime = 4.0f;
	private float blinkFrequency = 0.075f;
	

	void Awake () {

        playerBody = transform.Find("PlayerBody").gameObject;
        //driftEffect = playerBody.transform.FindChild("DrifingEffect").GetCompo;

		animContainer = playerBody.transform.Find("AnimationContainer").gameObject;
		playerBodyAnim = animContainer.GetComponent<Animation>();

		lifes2 = animContainer.transform.Find("PlayerLife2").gameObject;
		lifes3 = animContainer.transform.Find("PlayerLife3").gameObject;
		playerBaseRing = animContainer.transform.Find("Player_Mesh").gameObject;
		lifeCounter3 = transform.Find("AnimLifesLeft3").gameObject;
		lifeCounter2 = transform.Find("AnimLifesLeft2").gameObject;
		lifeCounter1 = transform.Find("AnimLifesLeft1").gameObject;
		lifeCounter0 = transform.Find("AnimLifesLeft0").gameObject;
		playerFlash_Alpha = transform.Find("PlayerFlash_Alpha").gameObject;

		projectorLight = transform.Find("Blob Light Projector").GetComponent<Projector>();
		
		boostTimer = boostControllerPressTime;
		
		playerRigidbody = GetComponent<Rigidbody>();
		
		//arrow = transform.Find("Arrow").gameObject;
		arrows = new GameObject[] {
			transform.Find("ArrowP1").Find("Mesh").gameObject,
			transform.Find("ArrowP2").Find("Mesh").gameObject,
			transform.Find("ArrowP3").Find("Mesh").gameObject,
			transform.Find("ArrowP4").Find("Mesh").gameObject
		};

		
		powerupTimer = powerupDuration;
		
		startScale = transform.localScale;

		moveEffect.enableEmission = false;
		
		//currentLifeSign.renderer.material.color = playerColor.color;
		//arrow.renderer.material.color = playerColor.color;
		//playerLight.color = playerColor.color;
		//projectorLight.material.color = playerColor.color;
		
		
		
		
		
		
	}
	
	void Start () {


		//InitPlayer();
		
	}

	public void InitPlayer(int playerID){

        
		currentPlayerIndex = playerID;

		// change arrow
		GetPlayerSymbol();

		//projectorLight.material = playerColor;

		// get from levelmanager?
		P1_spawnLocation = GameObject.Find("P1_spawnLocation");
		P2_spawnLocation = GameObject.Find("P2_spawnLocation");
		P3_spawnLocation = GameObject.Find("P3_spawnLocation");
		P4_spawnLocation = GameObject.Find("P4_spawnLocation");


		mainGameScript = GameManager.instance;

		//TODO: this one at a later point?
		StartAnimation();

        

		playerInitialised = true;
	}

	public void StartAnimation()
	{
		switch (currentPlayerIndex)
		{
		case 1 :
			ChangePlayerLife(GameManager.instance.player1score);
			break;
		case 2 :
			ChangePlayerLife(GameManager.instance.player2score);
			break;
		case 3 :
			ChangePlayerLife(GameManager.instance.player3score);
			break;
		case 4 :
			ChangePlayerLife(GameManager.instance.player4score);
			break;
		}
		
	}
	
	// Update is called once per frame
	void Update () {

		if(rememberInputTimer > 0)
			rememberInputTimer -= Time.deltaTime;

		if(rememberInputTimer <= 0)
		{
			//rememberedInput = Vector2.zero;
		}

		if(playerInitialised)
		{
			if(playerCanMove)
			{

				arrow.transform.localPosition = new Vector3(SerialInput.GetDir(currentPlayerIndex).x * arrowDistance, -0.5f, SerialInput.GetDir(currentPlayerIndex).y * arrowDistance);

				if (SerialInput.GetDir(currentPlayerIndex).x == 0 && SerialInput.GetDir(currentPlayerIndex).y == 0)
				{ 
					resetBoostTimer();
					if(rememberInputTimer <= 0)
					{
						rememberedInput = Vector2.zero;
					}
				}
				else
				{ 
					if(allowBoost) 
					{	
						boostTimerEnabled = true;
					}
					// remember input for a short time!
					rememberInputTimer = rememberInputTime;
					rememberedInput = SerialInput.GetDir(currentPlayerIndex);
				}

				//ghost
				if (beatlessControls) { 
					transform.Translate(new Vector3(SerialInput.GetDir(currentPlayerIndex).x * 10, 0, SerialInput.GetDir(currentPlayerIndex).y * 10) * Time.deltaTime); 
				}
				
				pulseSpeed = modifiablePulseSpeed;
			}

			if (boostTimerEnabled)
			{
				boostTimer -= Time.deltaTime;
				if (boostTimer > 0)
				{
					boostEnabled = true;
					pulseSpeed = modifiablePulseSpeed * 2;
					allowBoost = false;
				}
				else
				{
					allowBoost = false;
					boostTimerEnabled = false;
					boostEnabled = false;
					pulseSpeed = modifiablePulseSpeed;
					boostTimer = boostControllerPressTime;
				}
			}
				
			if(runPowerupTimer)
			{
				powerupTimer -= Time.deltaTime;
				if(powerupTimer < 0)
				{			
					runPowerupTimer = false;
					ResetPowerups();
					powerupTimer = powerupDuration;
				}
			}
			

		}

		moveEffect.enableEmission = GetComponent<Rigidbody>().velocity.magnitude > 5 ? true : false;

	}
	
	public void MovePlayerOnBeat()
	{

		if(playerCanMove)
		{
			Vector3 forcePlayer = new Vector3();
			//forcePlayer = new Vector3(SerialInput.GetDir(currentPlayerIndex).x, 0, SerialInput.GetDir(currentPlayerIndex).y);
			forcePlayer = new Vector3(rememberedInput.x, 0, rememberedInput.y);
			forcePlayer = forcePlayer.normalized;
			
			playerRigidbody.AddForce(forcePlayer * pulseSpeed);
			
			if (boostEnabled == true)
			{
				Vector3 pulseDirection = (transform.position + forcePlayer) - transform.position;

				//TODO: ugly, object pool! recycle!
				GameObject boostPulse1 = (GameObject)Instantiate(Resources.Load("DashParticles"), transform.position + forcePlayer, Quaternion.LookRotation(pulseDirection));
				//boostPulse1.transform.FindChild("Dash").particleSystem.renderer.material.SetColor("_TintColor", playerColorBasedOnBG);
				//boostPulse1.transform.FindChild("Speedrings").particleSystem.startColor = playerColorBasedOnBG;
				//boostPulse1.transform.FindChild("Speedrings").particleSystem.renderer.material.SetColor("_TintColor", playerColorBasedOnBG);
				Destroy(boostPulse1, boostPulse1.transform.Find("Dash").GetComponent<ParticleSystem>().startLifetime);

				MusicManager.instance.soundfx_Dash.PlayOneShot(MusicManager.instance.soundfx_Dash.clip);
			}

	        driftEffect.Play();
			arrow.GetComponent<Animation>().Play("Player_Arrow_Beat", PlayMode.StopAll);

			if(forcePlayer != Vector3.zero)
			{
				Quaternion playerRot = Quaternion.LookRotation(forcePlayer,Vector3.up);
				playerBody.transform.rotation = playerRot;
				playerBodyAnim.Play("PlayerBody_Move");
			}
		}
	}
	
	void startBoostTimer()
	{
		boostTimerEnabled = true;
	}
	
	void resetBoostTimer()
	{
		allowBoost = true;
		
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.name == "ExitArea")
		{
			playerRigidbody.velocity = Vector3.zero;
			Instantiate(Resources.Load("Explosion"),gameObject.transform.position, Quaternion.Euler(-90, 0, 0));
			
			switch(gameObject.name)
			{
			case "Player1" :
				mainGameScript.player1score--;
				if(mainGameScript.player1score > 0)
				{
					ResetPlayer();
					GameObject respawnPulse = (GameObject)Instantiate(Resources.Load("RespawnPulseV2"),transform.position, Quaternion.identity);
					Destroy(respawnPulse, respawnPulse.GetComponent<ParticleSystem>().startLifetime);
					//mainGameScript.soundfx_LoseLife.audio.PlayOneShot(mainGameScript.soundfx_LoseLife.audio.clip);
					
				}
				else
				{ExplodePlayer(gameObject);}
				break;
			case "Player2" :
				mainGameScript.player2score--;
				if(mainGameScript.player2score > 0)
				{
					ResetPlayer();
					GameObject respawnPulse = (GameObject)Instantiate(Resources.Load("RespawnPulseV2"), transform.position, Quaternion.identity);
					Destroy(respawnPulse, respawnPulse.GetComponent<ParticleSystem>().startLifetime);
					//mainGameScript.soundfx_LoseLife.audio.PlayOneShot(mainGameScript.soundfx_LoseLife.audio.clip);
				}
				else
				{ExplodePlayer(gameObject);}
				break;
			case "Player3" :
				mainGameScript.player3score--;
				if(mainGameScript.player3score > 0)
				{
					ResetPlayer();
					GameObject respawnPulse = (GameObject)Instantiate(Resources.Load("RespawnPulseV2"), transform.position, Quaternion.identity);
					Destroy(respawnPulse, respawnPulse.GetComponent<ParticleSystem>().startLifetime);
					//mainGameScript.soundfx_LoseLife.audio.PlayOneShot(mainGameScript.soundfx_LoseLife.audio.clip);
				}
				else
				{ExplodePlayer(gameObject);}
				break;
			case "Player4" :
				mainGameScript.player4score--;
				if(mainGameScript.player4score > 0)
				{
					ResetPlayer();
					GameObject respawnPulse = (GameObject)Instantiate(Resources.Load("RespawnPulseV2"), transform.position, Quaternion.identity);
					Destroy(respawnPulse, respawnPulse.GetComponent<ParticleSystem>().startLifetime);
					//mainGameScript.soundfx_LoseLife.audio.PlayOneShot(mainGameScript.soundfx_LoseLife.audio.clip);
				}
				else
				{ExplodePlayer(gameObject);}
				break;
			}

			MusicManager.instance.soundfx_LoseLife.PlayOneShot(MusicManager.instance.soundfx_LoseLife.clip);
			GameManager.instance.CheckScore();
		}
	}
	
	private void ChangePlayerLife(int _lifeCount){  

		ChangePlayerColors();

		switch (_lifeCount)
		{
		case 3:
			lifes3.SetActive(true);
			lifes2.SetActive(false);
			
			currentLifeSign = lifes3;
			
			lifeCounter0.SetActive(false);
			lifeCounter1.SetActive(false);
			lifeCounter2.SetActive(false);
			lifeCounter3.SetActive(true);
			break;
			
		case 2:
			lifes3.SetActive(false);
			lifes2.SetActive(true);
			
			currentLifeSign = lifes2;
			
			lifeCounter0.SetActive(false);
			lifeCounter1.SetActive(false);
			lifeCounter2.SetActive(true);
			lifeCounter3.SetActive(false);
			break;
			
		case 1:
			lifes3.SetActive(false);
			lifes2.SetActive(false);
			
			lifeCounter0.SetActive(false);
			lifeCounter1.SetActive(true);
			lifeCounter2.SetActive(false);
			lifeCounter3.SetActive(false);
			break;
		}
	}
	
	void ExplodePlayer(GameObject thisPlayer)
	{
		// MOAR EXPLOSIOOOONS?
		Instantiate(Resources.Load("Explosion"), gameObject.transform.position, Quaternion.Euler(-90, 0, 0));
		MusicManager.instance.soundfx_Explosion.PlayOneShot(MusicManager.instance.soundfx_Explosion.clip);
		thisPlayer.SetActive(false);

		playerInitialised = false; //TODO: does this mean problems?
	}
	
	void OnCollisionEnter(Collision collisionInfo)
	{
		if(collisionInfo.collider.gameObject.tag == "CollPlayer" && canBounceOthers == true)
		{

			StartCoroutine("BounceFlashPlayer");

			GameObject bumpGO1 = (GameObject)Instantiate(Resources.Load("RespawnPulseV2"),collisionInfo.transform.position,Quaternion.identity);
			GameObject bumpGO2 = (GameObject)Instantiate(Resources.Load("RespawnPulseV2"), this.transform.position, Quaternion.identity);
			bumpGO1.GetComponent<ParticleSystem>().startColor = playerColorBasedOnBG;
			bumpGO2.GetComponent<ParticleSystem>().startColor = playerColorBasedOnBG;

			Destroy(bumpGO1, bumpGO1.GetComponent<ParticleSystem>().startLifetime);
			Destroy(bumpGO2, bumpGO2.GetComponent<ParticleSystem>().startLifetime);

			MusicManager.instance.soundfx_Bump.PlayOneShot(MusicManager.instance.soundfx_Bump.clip);

			CameraManager.instance.DoScreenShake();

			GameObject hitFX = (GameObject)Instantiate(hitEffect,collisionInfo.contacts[0].point + new Vector3(0,-3,0),Quaternion.LookRotation(GetComponent<Rigidbody>().velocity));
			Destroy(hitFX,hitFX.GetComponent<ParticleSystem>().startLifetime);

			Quaternion playerRot = Quaternion.LookRotation(transform.position - collisionInfo.collider.transform.position,Vector3.up);
			playerBody.transform.rotation = playerRot;
			//playerBodyAnim.Play("PlayerBody_Bounce"); //maybe


			// Extra physics?
			// nope not needed
			/*
			collisionInfo.collider.gameObject.rigidbody.velocity = -collisionInfo.collider.gameObject.rigidbody.velocity * 1.5f;
			rigidbody.velocity = -rigidbody.velocity * 1.5f;
			*/
			
			// Timescale dip
			//GameManager.instance.timeScale = 0.5f;
			GameManager.instance.DoTimeScaleDip();

		}
		
		if (collisionInfo.collider.tag == "Cage")
		{
			collisionInfo.transform.parent.GetComponent<PowerUp_Cage>().FirstHit();
			//GameObject _obj = (GameObject)Instantiate(collisionInfo.gameObject.GetComponent<Cage>().hiddenToken);
			
			//_obj.transform.position = collisionInfo.transform.position;
			
			//Destroy(collisionInfo.collider.gameObject);
		}
		
		if (playerCanPickupPowerups)
		{
			
			if (collisionInfo.collider.gameObject.tag == "GrowPickup")
			{
				if (amountOfGrowPowerups < 3)
				{
					playerRigidbody.isKinematic = false;
					playerRigidbody.velocity = Vector3.zero;
					playerRigidbody.isKinematic = true;
					amountOfGrowPowerups++;
					iTween.ScaleTo(gameObject, iTween.Hash("scale", transform.localScale + (new Vector3(1.5f, 0, 1.5f) * amountOfGrowPowerups), "easetype", "easeOutElastic", "time", 0.35f, "oncomplete", "SetGrowPowerupSettings")); //new
					//Destroy(collisionInfo.collider.gameObject);
					collisionInfo.gameObject.transform.parent.GetComponent<PowerUp_Cage>().SecondHit();
					powerupTimer = powerupDuration;
					runPowerupTimer = true;
				}
			}
			
			if (collisionInfo.collider.gameObject.tag == "FasterPickup")
			{
				playerRigidbody.isKinematic = true;
				amountOfFasterPowerups++;
				iTween.PunchScale(gameObject, iTween.Hash("amount", new Vector3(1, 0, 1), "easetype", "easeOutElastic", "time", 0.35f, "oncomplete", "SetFasterPowerupSettings"));
				//Destroy(collisionInfo.collider.gameObject);
				//iTween.ColorTo(transform.FindChild("Arrow").gameObject,Color.blue,0.35f);
				powerupTimer = powerupDuration;
				runPowerupTimer = true;
			}
			
			if (collisionInfo.collider.gameObject.tag == "GhostPickup")
			{
				playerRigidbody.isKinematic = true;
				iTween.PunchScale(gameObject, iTween.Hash("amount", new Vector3(1, 0, 1), "easetype", "easeOutElastic", "time", 0.35f, "oncomplete", "SetGhostPowerupSettings"));
				//Destroy(collisionInfo.collider.gameObject);
				//iTween.ColorTo(transform.FindChild("Arrow").gameObject, Color.white, 0.35f);
				powerupTimer = powerupDuration;
				runPowerupTimer = true;
			}
		}
	}
	
	void SetFasterPowerupSettings()
	{
		playerRigidbody.isKinematic = false;
		playerRigidbody.mass = 1;
		modifiablePulseSpeed = normalPulseSpeed + normalPulseSpeed * amountOfFasterPowerups;
		//pulseSpeed = modifiablePulseSpeed;
		CameraManager.instance.screenshakeMagnitude = 0.5f;
	}
	
	void SetGrowPowerupSettings()
	{
		
		playerRigidbody.isKinematic = false;
		playerRigidbody.mass = 1 + 2*amountOfGrowPowerups;
		modifiablePulseSpeed = normalPulseSpeed + (3 * normalPulseSpeed)*amountOfGrowPowerups;
		//modifiablePulseSpeed = (normalPulseSpeed * 4) + (normalPulseSpeed * (amountOfGrowPowerups + 1));
		//pulseSpeed = modifiablePulseSpeed;
		CameraManager.instance.screenshakeMagnitude = 0.5f;
	}
	
	void SetGhostPowerupSettings()
	{
		//amountOfGhostPowerups++;
		playerRigidbody.isKinematic = true;
		GetComponent<Collider>().enabled = false;
		canBounceOthers = false;
		beatlessControls = true;
		
		// maybe just slowly float around instead of on the beat?
		
		//rigidbody.mass++;
		//mainGameScript.pulseSpeed2 = 3000.0f  * amountOfGrowPowerups;
		//mainGameScript.normalPulseSpeed = 3000.0f  * amountOfGrowPowerups;
		//mainGameScript.screenshakeMagnitude = 0.5f;


		// blinking effect?
		DoBlink(blinkTime);
	}
	
	public void ResetPowerups()
	{
		runPowerupTimer = false;
		powerupTimer = powerupDuration;
		amountOfGrowPowerups = 0;
		amountOfFasterPowerups = 0;
		GetComponent<Collider>().enabled = true;
		canBounceOthers = true;
		playerRigidbody.isKinematic = false;
		beatlessControls = false;
		pulseSpeed = normalPulseSpeed;
		modifiablePulseSpeed = normalPulseSpeed;
		playerRigidbody.mass = 1;
		
		//Debug.Log("localscale: " + transform.localScale + " startscale: " + startScale);
		//transform.localScale = startScale;
		
		playerCanPickupPowerups = false;
		iTween.ScaleTo(gameObject, iTween.Hash("scale",startScale,"easetype","easeOutElastic","time",0.25f,"oncomplete","FinishedScalingPlayerBackToNormal","oncompletetarget",this.gameObject));
		
		//iTween.ColorTo(transform.FindChild("Arrow").gameObject, playerColor.color, 0.35f);
		//mainGameScript.screenshakeMagnitude = 0.25f;
	}
	
	public void ResetPlayer()
	{
		P1_spawnLocation = GameObject.Find("P1_spawnLocation");
		P2_spawnLocation = GameObject.Find("P2_spawnLocation");
		P3_spawnLocation = GameObject.Find("P3_spawnLocation");
		P4_spawnLocation = GameObject.Find("P4_spawnLocation");

		
		gameObject.SetActive(true);
		ResetPowerups();
		
		ResetPosition();
		StartAnimation();

		FreezePlayerOnReset();
		DoBlink(PlayerManager.instance.playerResetTime);
		
		//currentLifeSign.renderer.material.color = playerColor.color;
		//arrow.renderer.material.color = playerColor.color;
		//playerLight.color = playerColor.color;
		//projectorLight.material = playerColor;
	}
	
	public void ResetPosition()
	{
		playerRigidbody.velocity = Vector3.zero;
		GetComponent<Rigidbody>().isKinematic = true;
		GetComponent<Collider>().enabled = false;
		if (gameObject.name == "Player1") { transform.position = P1_spawnLocation.transform.position; }
		if (gameObject.name == "Player2") { transform.position = P2_spawnLocation.transform.position; }
		if (gameObject.name == "Player3") { transform.position = P3_spawnLocation.transform.position; }
		if (gameObject.name == "Player4") { transform.position = P4_spawnLocation.transform.position; }
		GetComponent<Rigidbody>().isKinematic = false;
		GetComponent<Collider>().enabled = true;
		playerRigidbody.velocity = Vector3.zero;
		
	}
	
	private void FinishedScalingPlayerBackToNormal()
	{
		playerCanPickupPowerups = true;
	}
	
	public void ChangePlayerColors()
	{	
		playerBaseRing.GetComponent<Renderer>().material.color = GraphicsManager.instance.playerColorBasedOnBG;
		lifes3.GetComponent<Renderer>().material.color = playerColor.color;
		lifes2.GetComponent<Renderer>().material.color = playerColor.color;
		arrow.GetComponent<Renderer>().material.color = GraphicsManager.instance.playerColorBasedOnBG;
		playerLight.color = playerColor.color;
		projectorLight.material = playerColor;

	}

	public void FreezePlayerOnReset()
	{
		StartCoroutine("FreezePlayerOnResetCoroutine");

	}

	IEnumerator FreezePlayerOnResetCoroutine()
	{
		float freezeTimer = 0.0f;
		while(freezeTimer < PlayerManager.instance.playerResetTime)
		{

			playerCanMove = false;
			GetComponent<Rigidbody>().isKinematic = true;
			GetComponent<Collider>().isTrigger = true;
			
			freezeTimer += Time.deltaTime;
			
			yield return 0;
		}
		UnFreezePlayerOnReset();
	}

	private void UnFreezePlayerOnReset()
	{
		playerCanMove = true;
		GetComponent<Rigidbody>().isKinematic = false;
		GetComponent<Collider>().isTrigger = false;
	}


	public void DoBlink(float blinkTimeParam = 0)
	{
		if(blinkTimeParam == 0)
			StartCoroutine("DoBlinkCoroutine");
		else
			StartCoroutine("DoBlinkCoroutine",blinkTimeParam);

	}

	IEnumerator DoBlinkCoroutine(float blinkTimeParam = 0)
	{
		float blinkingTime;
		if(blinkTimeParam == 0)
		{
			blinkingTime = blinkTime; // default
		}
		else
		{
			blinkingTime = blinkTimeParam;
		}

		float blinkTimer = 0.0f;
		float switchTimer = 0.0f;
		bool toggleTransparency = true;

		while(blinkTimer < blinkingTime)
		{
			blinkTimer += Time.deltaTime;

			switchTimer += Time.deltaTime;
			if(switchTimer > blinkFrequency)
			{
				toggleTransparency = !toggleTransparency;
				switchTimer = 0;
			}
			 

			//int ceilTimer = Mathf.CeilToInt(blinkTimer);

			foreach(Transform mesh in animContainer.transform)
			{
				Color tempColor = mesh.GetComponent<Renderer>().material.color;

				if(toggleTransparency)
				{
					tempColor.a = 0.2f;
				}
				else
				{
					tempColor.a = 1.0f;
				}


				mesh.GetComponent<Renderer>().material.color = tempColor;
			}




			//yield return new WaitForSeconds(blinkFrequency);

			yield return 0;
		}


		StopBlink();

		yield return 0;
	}

	public void StopBlink()
	{
		foreach(Transform mesh in animContainer.transform)
		{
			//mesh.renderer.enabled = true;
			Color tempColor = mesh.GetComponent<Renderer>().material.color;
			tempColor.a = 1.0f;
			mesh.GetComponent<Renderer>().material.color = tempColor;
		}

	}

	private void GetPlayerSymbol(){

		// all symbols off
		foreach(GameObject arrowGO in arrows)
		{
			arrowGO.SetActive(false);
		}

		arrows[currentPlayerIndex - 1].SetActive(true);
		arrow = arrows[currentPlayerIndex - 1];
	}

	IEnumerator BounceFlashPlayer()
	{
		Material flashMaterial = playerFlash_Alpha.GetComponent<Renderer>().material;
		Color flashMaterialColor = new Color(1,1,1,1);
		flashMaterial.color = flashMaterialColor;

		while(flashMaterialColor.a > 0)
		{
			flashMaterialColor.a -= Time.deltaTime * 7.5f;
			flashMaterial.color = flashMaterialColor;
			yield return 0;
		}

	}
}
