﻿using UnityEngine;
using System.Collections;

public class PowerUp_Cage : MonoBehaviour {


    public GameObject hiddenToken;

    public float minVelocity;

    public bool hit=false;
	
	private GameObject cage;
	private GameObject pickup;

	private void Awake()
	{

		cage = transform.Find("cage").gameObject;
		pickup = transform.Find("pickup").gameObject;

		pickup.SetActive(false);

	}
	
	public void FirstHit()
	{
		cage.SetActive(false);
		pickup.SetActive(true);
	}

	public void SecondHit()
	{
		//cage.SetActive(false);
		//pickup.SetActive(false);
		PowerupManager.instance.DestroyPickup(gameObject);

	}
}
