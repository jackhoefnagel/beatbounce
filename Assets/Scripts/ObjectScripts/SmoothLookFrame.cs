﻿using UnityEngine;
using System.Collections.Generic;

public class SmoothLookFrame : MonoBehaviour {
 
    public float damping = 2.0f;
	
	public Vector3 offsetRotation = Vector3.zero;
	public Vector3 offsetPosition = Vector3.zero;
	
	private Vector3 startPosition;
 
	public GameObject player1;
	public GameObject player2;
	public GameObject player3;
	public GameObject player4;

    private List<GameObject> playersArray;
	
	private bool beginAnimationPlaying;

    public float startAnimTime;

    public GameObject centerObject;

    public string levelName;

    public bool doScreenShake = false;
    public float screenShakeMagnitude = 1.0f;
	
	void Start()
	{
		player1 = GameObject.Find("Player1");
		player2 = GameObject.Find("Player2");
		player3 = GameObject.Find("Player3");
		player4 = GameObject.Find("Player4");

        playersArray = new List<GameObject>();
        playersArray.Add(player1);
        playersArray.Add(player2);
        playersArray.Add(player3);
        playersArray.Add(player4);
        
		startPosition = transform.position;

        transform.position = startPosition + offsetPosition;
        
		
		BeginAnimation();
	}
 
    void Update () {
		if(!beginAnimationPlaying)
		{

            Vector3 playersCenterPosition = new Vector3();
            int amountOfPlayersActive = 0;
            foreach (GameObject player in playersArray)
            {
                if (player.activeSelf)
                {
                    playersCenterPosition += player.transform.position;
                    amountOfPlayersActive++;
                }
            }

            playersCenterPosition += offsetRotation;
            playersCenterPosition /= amountOfPlayersActive+1;

            float tempYOffset = 0;
            foreach (GameObject player in playersArray)
            {
                if (player.activeSelf)
                {
                   float tempDistance = Vector3.Distance(player.transform.position, playersCenterPosition);
                   if (tempDistance> tempYOffset)
                   {
                       tempYOffset = tempDistance;
                   }
                }
            }


            offsetPosition = new Vector3(0,tempYOffset+5.0F,0);

            //Reset the extremes. 
            float[] extremes = new float[4] { 100000, -100000, 100000, -100000 };

            //For each player check if he is one of the extremes.
            for (int i = 0; i < playersArray.Count; i++)
            {
                //Player on the far left
                if (playersArray[i].transform.position.x < extremes[0])
                    extremes[0] = playersArray[i].transform.position.x;
                //Player on the far right
                if (playersArray[i].transform.position.x > extremes[1])
                    extremes[1] = playersArray[i].transform.position.x;
                //Player on the bottom. Players that are falling to death not included.
                if (playersArray[i].transform.position.z < extremes[2] )
                    extremes[2] = playersArray[i].transform.position.z;
                //Player on the top
                if (playersArray[i].transform.position.z > extremes[3])
                    extremes[3] = playersArray[i].transform.position.z;
            }

            //The distance between the player on the very bottom to the the player pos. on the top.
            float heightDist = Mathf.Abs(extremes[3] - extremes[2]);

            //Same but in the width.
            float widthDist = Mathf.Abs(extremes[1] - extremes[0]) / this.GetComponent<Camera>().aspect;

            //Reset the orthagonal size, with which the the current will be smoothed afterwards.
            float newPerpSize = 0.0F;

            //Use the bigger distance ( height / width) for the calculations of the screensize.
            if (widthDist > heightDist)
            {
                newPerpSize = (widthDist) / 2.0F + 10.0F;
            }
            else
            {
                newPerpSize = heightDist / 2.0F + 15.0F;
            }
            newPerpSize = Mathf.Clamp(newPerpSize, 22.0F, 60.0F);

            this.GetComponent<Camera>().orthographicSize = iTween.FloatUpdate(this.GetComponent<Camera>().orthographicSize, newPerpSize, 0.15F);

            offsetPosition = Vector3.zero;

            //centerObject.transform.position = playersCenterPosition;


			//Vector3 playersCenterPosition = (player1.transform.position + player2.transform.position + player3.transform.position + player4.transform.position + offsetRotation)/5;
			
			Vector3 look = playersCenterPosition - transform.position;
			
	        Quaternion rotate = Quaternion.LookRotation(look,Vector3.forward);
		    transform.rotation = Quaternion.Slerp(transform.rotation, rotate, Time.deltaTime * damping);

            //transform.position = startPosition + offsetPosition;

            Vector3 camPosition;

            if (!doScreenShake)
            {
                 camPosition = startPosition + new Vector3(playersCenterPosition.x, 0, playersCenterPosition.z) + offsetPosition;
            }
            else
            {
                camPosition = startPosition + new Vector3(playersCenterPosition.x, 0, playersCenterPosition.z) + offsetPosition + new Vector3(Random.Range(-screenShakeMagnitude, screenShakeMagnitude), Random.Range(-screenShakeMagnitude, screenShakeMagnitude), Random.Range(-screenShakeMagnitude, screenShakeMagnitude));
            }

            iTween.MoveUpdate(gameObject, camPosition, 1.0f);
            //transform.position = startPosition + new Vector3(playersCenterPosition.x, 0, playersCenterPosition.z) + offsetPosition;
		}
    }
	
	void BeginAnimation()
	{
		beginAnimationPlaying = true;
        switch (levelName)
        {
            case "Savant" :
                iTween.MoveFrom(gameObject, iTween.Hash("position", new Vector3(0.0f, -180f, -140), "easetype", "easeInOutQuad", "time", startAnimTime));
                iTween.RotateFrom(gameObject, iTween.Hash("rotation", new Vector3(-45f, 0.0f, 0.0f), "easetype", "easeInOutQuad", "time", startAnimTime, "oncomplete", "BeginAnimationFinished"));
                break;
            default :
                iTween.MoveFrom(gameObject, iTween.Hash("position", new Vector3(0.0f, 12.42524f, -52.53772f), "easetype", "easeInOutQuad", "time", startAnimTime));
                iTween.RotateFrom(gameObject, iTween.Hash("rotation", new Vector3(16.96553f, 0.0f, 0.0f), "easetype", "easeInOutQuad", "time", startAnimTime, "oncomplete", "BeginAnimationFinished"));
                break;
        }
	}
	
	public void BeginAnimationFinished()
	{
		beginAnimationPlaying = false;
	}
}