﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System.Threading;


public class SerialInput : MonoBehaviour
{
    //private bool[] bits;
    private static SerialInput iLL;
    private static bool _continue = true;
    private Thread newThread;
    private static bool[] inputs = new bool[16];

    public static bool debugInput = false;

    void Awake()
    {
        iLL = this;

        newThread = new Thread( new ThreadStart(SerialHandler));
        newThread.Start();

        DontDestroyOnLoad(this.gameObject);

    }

    private static void SerialHandler()
    {
        // Get a list of serial port names. 
        string[] ports = SerialPort.GetPortNames();

        Debug.Log("The following serial ports were found:");

        // Display each port name to the console. 
        foreach (string portys in ports)
        {
            Debug.Log(portys);
        }

        SerialPort port = new SerialPort("COM9", 9600);

        port.Open();
        if (!port.IsOpen)
        {
            Debug.LogError("wrong SerialPort");
            return;
        }
        while (_continue)
        {
            try
            {
                string message = port.ReadLine();
                Debug.Log(message);

                ProcessData(message);

            }
            catch (System.Exception e) { 
            
                Debug.LogException(e, iLL);
            }
            //System.Threading.Thread.Sleep(100);
        }
    
        port.Close();
        Debug.Log("Thread eliminated! Fuck yeah!");
    }

    private static void ProcessData(string data)
    {

        if (data != null && data.Length >= 3) // If there is valid data insode the String
        {
            print(data);
            data.Trim(); // Remove whitespace characters at the beginning and end of the string
            string[] seperateValues = data.Split(','); // Split the string everytime a delimiter is received    

            foreach (string _s in seperateValues)
            {
                //print(_s);
            }
        //print(seperateValues[0] + " and " + seperateValues[1]);

        int firstValue;
		int secondValue ;
		bool firstBool = int.TryParse(seperateValues[0], out firstValue);
             bool secondBool = int.TryParse(seperateValues[1], out secondValue);

            int j = 0;
            string bits = "";

            for (int i = 0; i < 8; ++i)
            {
                if (i == 4)
                {
                    bits += "||";
                }

                inputs[j] = ((firstValue & (1 << i)) > 0);

                bits += inputs[j] ? '+' : '-';

                ++j;
            }
            bits += "||";
            for (int i = 0; i < 8; ++i)
            {
                if (i == 4)
                {
                    bits += "||";
                }

                inputs[j] = ((secondValue & (1 << i)) > 0);

                bits += inputs[j] ? '+' : '-';

                ++j;
            }

            //print(bits);
        }


    }

    public static Vector2 GetDir(int controllerId)
    {
        //controllerId++; //Hacky?
        //Basic
        Vector2 moveVec = Vector2.zero;
        float minInput = 0.2F;

        //Keyboard
        moveVec = new Vector2(Input.GetAxisRaw("KeyboardHorizontal" + controllerId), Input.GetAxisRaw("KeyboardVertical" + controllerId));

        //XBOX
        if (Input.GetAxis("XboxHorizontal" + controllerId) > minInput || Input.GetAxis("XboxHorizontal" + controllerId) < -minInput)
        {
            moveVec.x += Input.GetAxis("XboxHorizontal" + controllerId);
        }

        if (Input.GetAxis("XboxVertical" + controllerId) > minInput || Input.GetAxis("XboxVertical" + controllerId) < -minInput)
        {
            moveVec.y += Input.GetAxis("XboxVertical" + controllerId);
        }

        if (Input.GetAxis("XboxHorizontalALT" + controllerId) > minInput || Input.GetAxis("XboxHorizontalALT" + controllerId) < -minInput)
        {
            moveVec.x += Input.GetAxis("XboxHorizontalALT" + controllerId);
        }

        if (Input.GetAxis("XboxVerticalALT" + controllerId) > minInput || Input.GetAxis("XboxVerticalALT" + controllerId) < -minInput)
        {
            moveVec.y += Input.GetAxis("XboxVerticalALT" + controllerId);
        }

        if(moveVec != Vector2.zero)
            return new Vector2(Mathf.Clamp(moveVec.x, -1, 1), Mathf.Clamp(moveVec.y, -1, 1));


        controllerId -= 1;
        if (controllerId == 3)
        {
            int left = inputs[controllerId * 4] ? 1 : 0;
            int right = inputs[controllerId * 4 + 1] ? 1 : 0;
            int up = inputs[controllerId * 4 + 2] ? 1 : 0;
            int down = inputs[controllerId * 4 + 3] ? 1 : 0;
            //print(right +" " + left + " and "+ up +" " +down);
            return new Vector2(right - left, up - down);
        }
        else
        {
            int down = inputs[controllerId * 4] ? 1 : 0;
            int up = inputs[controllerId * 4 + 1] ? 1 : 0;
            int right = inputs[controllerId * 4 + 2] ? 1 : 0;
            int left = inputs[controllerId * 4 + 3] ? 1 : 0;
            //print(right +" " + left + " and "+ up +" " +down);
            return new Vector2(right - left, up - down);
        }
        

    }


	void Start ()
    {
	
	}
	void Update ()
    {
	
	}

    void OnDestroy()
    {
        _continue = false;
        while (newThread.IsAlive)
        {
            System.Threading.Thread.Sleep(100);
        }
    }




}
