﻿using UnityEngine;
using System.Collections;

public class BeatMarkerObject : MonoBehaviour {

	private float tempoTimer;
	public bool shouldMove = false;
	
	private float dspTimer;
	private float animDuration;
	private Vector3 destination = Vector3.zero;
	
	public GameObject audioObject;


	public float timeUntilEnd = 4.0f;

	// Use this for initialization
	void Start () {
		StartCoroutine("RunDSPTimer");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator RunDSPTimer () {

			//float startDSPtime = (float)AudioSettings.dspTime;

			dspTimer = (float)AudioSettings.dspTime;
			
		//while ( (dspTimer - startDSPtime) < timeUntilEnd )
		while ( true )
			{
				float difference = (float)AudioSettings.dspTime - dspTimer;
				Vector3 pos = transform.position;

				pos.x -= difference * 4;

				transform.position = pos;
				dspTimer = (float)AudioSettings.dspTime;
				animDuration += Time.deltaTime;
				yield return 0;
			}
			
			//Debug.Log("end time: "+animDuration);
		}
}
