﻿using UnityEngine;
using System.Collections;

public class ParticleSystemSwitch : MonoBehaviour {

    public ParticleSystem[] allSystems;

       public Vector2 waitRandomRange;

	// Use this for initialization
	private void Start () {

        allSystems = this.GetComponentsInChildren<ParticleSystem>();

        StartCoroutine(WaitForSwitch());
	}

    private IEnumerator WaitForSwitch()
    {
        yield return new WaitForSeconds(Random.Range(waitRandomRange.x, waitRandomRange.y));

        Switch();
    }

	private void Switch () {
        for (int i = 0; i < allSystems.Length; i++)
        {
            allSystems[i].Pause();
        }

        allSystems[Random.Range(0, allSystems.Length)].Play();

        StartCoroutine(WaitForSwitch());
	}
}
