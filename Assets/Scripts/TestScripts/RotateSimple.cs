﻿using UnityEngine;
using System.Collections;

public class RotateSimple : MonoBehaviour {

    public float degrees = 30.0F; //per second
    public bool random;

    private Vector3 tempRotation = Vector3.zero;
    private float randomSpeedChange = 3.0F;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (random)
        {
            tempRotation.x += Random.RandomRange(-1.0F, 1.0F) * randomSpeedChange * Time.deltaTime;
            tempRotation.y += Random.RandomRange(-1.0F, 1.0F) * randomSpeedChange * Time.deltaTime;
            tempRotation.z += Random.RandomRange(-1.0F, 1.0F) * randomSpeedChange * Time.deltaTime;

            this.transform.Rotate(tempRotation, degrees * Time.deltaTime);
        }
        else
        {
            this.transform.Rotate(Vector3.up, degrees * Time.deltaTime);
        }
	}
}
