﻿using UnityEngine;
using System.Collections;

public class SimpleMove : MonoBehaviour {

    public float moveSpeed;
    public Vector3 direction;
	
	// Update is called once per frame
	void Update () {
        this.transform.Translate(direction * moveSpeed);
	}
}
