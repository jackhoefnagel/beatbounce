﻿using UnityEngine;
using System.Collections;

public class Powerup_Bomb : MonoBehaviour {

	private bool hasBeenHit = false;
	public GameObject explosionPrefab;

	private Animator animator;

	void Awake()
	{
		animator = GetComponent<Animator>();
	}

	void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.tag == "CollPlayer")
		{
			if(hasBeenHit == false)
			{
				animator.SetTrigger("TriggerBomb");
				gameObject.GetComponent<Rigidbody>().AddForce(collision.relativeVelocity,ForceMode.VelocityChange);
				hasBeenHit = true;
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.name == "ExitArea")
		{
			transform.position = new Vector3(-3,5,11);
		}
	}

	public void ExplodeBomb ()
	{
		GameObject explosion = (GameObject)Instantiate(explosionPrefab,transform.position,Quaternion.Euler(-90,0,0));
		Destroy(explosion,explosion.GetComponent<ParticleSystem>().startLifetime);
		float power = 5000.0f;
		float radius = 20.0f;

		Vector3 explosionPos = transform.position;
		Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
		foreach (Collider hit in colliders) {
			if (hit && hit.GetComponent<Rigidbody>())
				hit.GetComponent<Rigidbody>().AddExplosionForce(power, explosionPos, radius, 0.0F);
			
		}
		Destroy(gameObject);
	}
}
