﻿using UnityEngine;
using System.Collections;

public class CheckArduino : MonoBehaviour {

    public GameObject arduinoInputPrefab;

	// Use this for initialization
	void Start () {


        if (!GameObject.Find("DirectArduinoInput"))
        {
            Instantiate(arduinoInputPrefab);
        }
	}
}
