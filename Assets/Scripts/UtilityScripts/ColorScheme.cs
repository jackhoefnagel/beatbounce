﻿using UnityEngine;
using System.Collections;

public class ColorScheme : MonoBehaviour {

	// Colors
	public Color levelSegmentsColor = Color.white;
	public Color levelSegmentsDisappearingColor = Color.white;
	public Color backgroundColor = Color.black;
	public Color backgroundGraphicsColorObject1 = Color.white;
	public Color backgroundGraphicsColorObject2 = Color.white;
	public Color backgroundGraphicsColorObject3 = Color.white;
}
