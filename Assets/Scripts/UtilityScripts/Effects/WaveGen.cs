﻿using UnityEngine;
using System.Collections;

public class WaveGen : MonoBehaviour
{
    public float scale = 0.1f;
    public float speed = 1.0f;
    public float noiseStrength = 1f;
    public float noiseWalk = 1f;

    private Vector3[] baseHeight;

    void Update()
    {
        Mesh mesh = GetComponent<MeshFilter>().mesh;

        if (baseHeight == null)
            baseHeight = mesh.vertices;

        Vector3[] vertices = new Vector3[baseHeight.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            Vector3 vertex = baseHeight[i];
            vertex.y += Mathf.Sin(Time.time * speed + baseHeight[i].x + baseHeight[i].y + baseHeight[i].z) * scale;
            vertex.y += (Mathf.PerlinNoise(baseHeight[i].x * Mathf.Sin(Time.time) * Random.Range(0.0F,2.0F), baseHeight[i].y * Time.deltaTime * Random.Range(0.0F, 50.0F)) * noiseStrength) * Time.deltaTime;
            vertices[i] = vertex;
        }
        mesh.vertices = vertices;
        mesh.RecalculateNormals();
    }
}