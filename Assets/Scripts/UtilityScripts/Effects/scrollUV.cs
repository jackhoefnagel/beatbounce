﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class scrollUV : MonoBehaviour
{
    public float scrollSpeed = 0.5F;
    void Update()
    {
        float offset = Time.time * scrollSpeed;

        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(offset, offset));
    }
}