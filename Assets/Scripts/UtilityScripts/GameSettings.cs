﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSettings : MonoBehaviour {

	[Header("Available Settings")]
	public GameObject[] availableLevelTypes;
	public GameObject[] availableBGGraphics;
	public GameObject[] availableColorSchemes;
	public GameObject[] availablePlaylists;
	public GameObject[] availableSongs;
	public GameObject[] availablePowerups;

	[Header("Actually Used Settings")]
	public GameObject[] chosenSongs;
	public GameObject[] chosenLevelTypes;
	public GameObject[] chosenBGGraphics;
	public GameObject[] chosenColorSchemes;
	public GameObject[] chosenPowerups;
	
	private List<ArrayList> rounds;
	private int roundIndex = 0;

	public GameObject chosenPlaylist;
	public bool playSongsRandom;
	public bool playSingleSong;
	public int firstSongIndex;

    public bool[] playersParticipating;

    public static GameSettings instance = null;
	void Awake(){
		if(instance == null)
		{
			instance = this;
            DontDestroyOnLoad(gameObject);
		}
        else if(instance != this){
            Destroy(this.gameObject);
            return;
        }

		// Default Settings
		chosenSongs = new GameObject[]{availableSongs[0]}; // necessary?
		chosenLevelTypes = availableLevelTypes;
		chosenBGGraphics = availableBGGraphics;
		chosenColorSchemes = availableColorSchemes;
		chosenPowerups = availablePowerups;

		chosenPlaylist = availablePlaylists[0];

		//TODO: put in powerups per # beats?
	}

	void Start()
	{

	}

	void Update()
	{
		//TODO: Debug key for reset game in gamesettings
		if(Input.GetKeyUp("o"))
		{
			NextRound();
			GameManager.instance.resetGame();
		}
	}

	public void MakePlaylist()
	{
		rounds = new List<ArrayList>();
		if(!playSingleSong)
		{
            for (int i = 0; i < chosenPlaylist.GetComponent<PlaylistScript>().playlistSongs.Count; i++)
            {

				int offsetIndex = i + firstSongIndex;
                if (offsetIndex > chosenPlaylist.GetComponent<PlaylistScript>().playlistSongs.Count - 1)
				{
                    offsetIndex -= chosenPlaylist.GetComponent<PlaylistScript>().playlistSongs.Count - 1;
				}

				ArrayList round = new ArrayList();
				round.Add((GameObject)chosenPlaylist.GetComponent<PlaylistScript>().playlistSongs[offsetIndex]); // song
				round.Add((GameObject)chosenLevelTypes[offsetIndex % chosenLevelTypes.Length]); // leveltype, rotate
				round.Add((GameObject)chosenBGGraphics[offsetIndex % chosenBGGraphics.Length]); // bggraphic, rotate
				round.Add((GameObject)chosenColorSchemes[offsetIndex % chosenColorSchemes.Length]); // colorscheme, rotate
				round.Add((GameObject)chosenPowerups[offsetIndex % chosenPowerups.Length]); // powerup, rotate?
				
				rounds.Add(round);
			}
		}
		else
		{
			// play single song
			ArrayList round = new ArrayList();
			round.Add((GameObject)chosenPlaylist.GetComponent<PlaylistScript>().playlistSongs[firstSongIndex]); // song
			round.Add((GameObject)chosenLevelTypes[firstSongIndex % chosenLevelTypes.Length]); // leveltype, rotate
			round.Add((GameObject)chosenBGGraphics[firstSongIndex % chosenBGGraphics.Length]); // bggraphic, rotate
			round.Add((GameObject)chosenColorSchemes[firstSongIndex % chosenColorSchemes.Length]); // colorscheme, rotate
			round.Add((GameObject)chosenPowerups[firstSongIndex % chosenPowerups.Length]); // powerup, rotate?
			
			rounds.Add(round);
		}

		if(playSongsRandom)
			rounds = ShufflePlaylist(rounds);

	}

	public void StartRound()
	{
		roundIndex = 0;
		InitRoundPrefabs();
	}

	public void NextRound()
	{
        if (roundIndex < chosenPlaylist.GetComponent<PlaylistScript>().playlistSongs.Count - 1)
		{
			roundIndex++;
			InitRoundPrefabs();

		}
		else
		{
			// return to menu
			Application.LoadLevel(0);
		}
	}

	private void InitRoundPrefabs()
	{		
		MusicManager.instance.selectedSongPrefab = (GameObject)rounds[roundIndex][0];
        //What should the thing do here????? Messes up testing
		//LevelManager.instance.selectedLevelPrefab = (GameObject)rounds[roundIndex][1];
		// leveltype hardcoded
		//LevelManager.instance.levelType = (firstSongIndex + roundIndex) % availableLevelTypes.Length; 
		GraphicsManager.instance.GraphicsPrefab = (GameObject)rounds[roundIndex][2];
		GraphicsManager.instance.ColorPrefab = (GameObject)rounds[roundIndex][3];
        //What should the thing do here?????
        //PowerupManager.instance.selectedPowerups[0] = (GameObject)rounds[roundIndex][4]; //TODO: powerup stuff, rotate? 

        GUIManager.instance.NewSong();
	}

	private List<ArrayList> ShufflePlaylist(List<ArrayList> playlist)
	{
		List<ArrayList> shuffledPlaylist;
		ArrayList temp;
		for (int i = 0; i < playlist.Count; i++) {
			temp = playlist[i];
			int swapIndex = Random.Range(0,(playlist.Count-1));
			playlist[i] = playlist[swapIndex];
			playlist[swapIndex] = temp;
		}
		shuffledPlaylist = playlist;
		return shuffledPlaylist;
	}


}
