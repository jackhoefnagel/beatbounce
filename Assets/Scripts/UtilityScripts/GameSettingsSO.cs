﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettingsSO : ScriptableObject {

    [Header("Level Settings")]
    public GameObject[] chosenSongs;
    public GameObject[] chosenLevelTypes;
    public GameObject[] chosenBGGraphics;
    public GameObject[] chosenColorSchemes;
    public GameObject[] chosenPowerups;
    public GameObject chosenPlaylist;

    public bool[] playersParticipating;

    [Header("Level Options")]
    public bool playSongsRandom;
    public bool playSingleSong;
    public int firstSongIndex;

    private List<ArrayList> rounds;
    private int roundIndex = 0;

}
