﻿using UnityEngine;
using System.Collections;

public class MusicConstantBeatsScript : MonoBehaviour {

	public float tempoBPM = 120.0f;
	private float tempoTimer = 0.5f; //120bpm
	
	void Start () {
		tempoTimer = 1 / (tempoBPM / 60);
		GUIManager.instance.HideIntroPopup();
	}

	void Update () {
		if (MusicManager.instance.endlessBeats == true)
		{
			tempoTimer -= Time.deltaTime;
			if (tempoTimer < 0)
			{
				MusicManager.instance.OnBeat();
				MusicManager.instance.soundfx_Dash.GetComponent<AudioSource>().Play();
				tempoTimer = 1 / (tempoBPM / 60);

			}			
		}
	}
}
