﻿using UnityEngine;
using System.Collections;

public class SongInfos : MonoBehaviour {

    public enum Genres
    {
        none,
        Chiptune,
        Metal,
        Disko

    }

    public enum SongMoods{

        none,
        Quiet,
        Hardcore,
        Danceable,
        Crazy,

    }
}
