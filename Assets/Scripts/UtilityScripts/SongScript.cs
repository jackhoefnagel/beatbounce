﻿using UnityEngine;
using System.Collections;

public class SongScript : MonoBehaviour {
	public AudioClip songClip;
	public TextAsset songXMLMarkers;

    //Sorting Info
	public string songName;
	public string songArtist;
    public SongInfos.Genres genre;
    public int bpm;
    public SongInfos.SongMoods mood;
    public int difficulty = 1; // 1-10
    public ColorScheme colorScheme;
    public GameObject levelPrefab;
    public GameObject bgGraphics;
    public GameObject[] powerups;
}
