﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using System.Collections;


public class XMfileParser  {

	private readonly byte[] _bytes;
	
	public XMfileParser(string filePath)
	{
		 
		_bytes = File.ReadAllBytes(filePath);
	}
	
	public string GetId
	{
		get
		{
			return BytesToString(ReadOffset(0x0, 17));
		}
	}
	
	public string GetModuleName
	{
		get
		{
			return BytesToString(ReadOffset(0x11, 20));
		}
	}
	
	public byte GetId01A
	{
		get
		{
			return ReadOffset(0x25)[0];
		}
	}
	
	public string GetTrackerName
	{
		get
		{
			return BytesToString(ReadOffset(0x26, 20));
		}
	}
	
	public short GetTrackerRevision
	{
		get
		{
			return BitConverter.ToInt16(ReadOffset(0x3A, sizeof(short)), 0);
		}
	}
	
	public int GetHeaderSize
	{
		get
		{
			return BitConverter.ToInt32(ReadOffset(0x3C, sizeof(int)), 0);
		}
	}
	
	public short GetSongPatternLength
	{
		get
		{
			return BitConverter.ToInt16(ReadOffset(0x40, sizeof(short)), 0);
		}
	}
	
	public short GetRestartPos
	{
		get
		{
			return BitConverter.ToInt16(ReadOffset(0x42, sizeof(short)), 0);
		}
	}
	
	public short GetNumChannels
	{
		get
		{
			return BitConverter.ToInt16(ReadOffset(0x44, sizeof(short)), 0);
		}
	}
	
	public short GetNumPatterns
	{
		get
		{
			return BitConverter.ToInt16(ReadOffset(0x46, sizeof(short)), 0);
		}
	}
	
	public short GetNumInstruments
	{
		get
		{
			return BitConverter.ToInt16(ReadOffset(0x48, sizeof(short)), 0);
		}
	}
	
	public short GetFlags
	{
		get
		{
			return BitConverter.ToInt16(ReadOffset(0x4A, sizeof(short)), 0);
		}
	}
	
	public short GetDefaultTempo
	{
		get
		{
			return BitConverter.ToInt16(ReadOffset(0x4C, sizeof(short)), 0);
		}
	}
	
	public short GetDefaultBpm
	{
		get
		{
			return BitConverter.ToInt16(ReadOffset(0x4E, sizeof(short)), 0);
		}
	}
	
	public byte[] GetPatternOrderTable
	{
		get
		{
			return ReadOffset(0x42, 256);
		}
	}
	
	private static string BytesToString(IEnumerable<byte> bytes)
	{
		//fixed (char* c = &bytes.Select(b => (char)b).ToArray()[0]) return new string();
		return "works";
	}
	
	private byte[] ReadOffset(int offset, int length = 1)
	{
		using (MemoryStream ms = new MemoryStream(_bytes, offset, length))
		{
			byte[] buffer = new byte[length];
			ms.Read(buffer, 0, length);
			return buffer;
		}
	}
}