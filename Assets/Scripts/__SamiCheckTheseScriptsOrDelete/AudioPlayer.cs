﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent( typeof( AudioSource ) )]
public class AudioPlayer : MonoBehaviour
{
	public float m_fVolumeMax = 0.8f;
	public float m_fFadeDuration = 1.0f;
	private enum EState{ STABLE, FADE_IN, FADE_OUT, _COUNT };
	private EState m_eState;
	private float m_fFadeSpeed;
	
	void Awake()
	{
		GetComponent<AudioSource>().loop = true;
		GetComponent<AudioSource>().playOnAwake = false;
		GetComponent<AudioSource>().volume = 0.0f;
		GetComponent<AudioSource>().Play();
		SetState( EState.STABLE );
	}
	void Start()
	{
	}
	void Update()
	{
		switch( m_eState )
		{
		case EState.STABLE:
			break;
		case EState.FADE_IN:
			GetComponent<AudioSource>().volume += m_fFadeSpeed * Time.deltaTime;
			if( GetComponent<AudioSource>().volume >= m_fVolumeMax )
			{
				GetComponent<AudioSource>().volume = m_fVolumeMax;
				SetState( EState.STABLE );
			}
			break;
		case EState.FADE_OUT:
			GetComponent<AudioSource>().volume -= m_fFadeSpeed * Time.deltaTime;
			if( GetComponent<AudioSource>().volume <= 0.0f )
			{
				GetComponent<AudioSource>().volume = 0.0f;
				SetState( EState.STABLE );
			}
			break;
		}
	}
	public void FadeIn()
	{
		if( IsStable() )
			SetState( EState.FADE_IN );
	}
	public void FadeOut()
	{
		if( IsStable() )
			SetState( EState.FADE_OUT );
	}
	public bool IsStable()
	{
		return m_eState == EState.STABLE;
	}
	private void SetState( EState eState )
	{
		switch( eState )
		{
		case EState.STABLE:
			break;
		case EState.FADE_IN:
			m_fFadeSpeed =
				( m_fVolumeMax - GetComponent<AudioSource>().volume )
					/ ( m_fFadeDuration );
			break;
		case EState.FADE_OUT:
			m_fFadeSpeed =
				GetComponent<AudioSource>().volume
					/ ( m_fFadeDuration );
			break;
		}
		m_eState = eState;
	}
}
