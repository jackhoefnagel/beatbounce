﻿using UnityEngine;
using System.Collections;

public class JumpingLight : MonoBehaviour {

    private Vector3 startPos;
    private Vector3 endPos;

    private Transform trans;

    private float direction;
    public float jumpSpeed;
    public bool movingLightON;

	// Use this for initialization
	void Start () {

        trans = this.transform;
        StartCoroutine(ChangeDirection());
	}
	
	// Update is called once per frame
	void Update () {
        if(movingLightON)
            trans.Translate((Vector3.up * jumpSpeed) * direction);
	}

    private IEnumerator ChangeDirection()
    {
        yield return new WaitForSeconds(1.8F);
        direction = direction * -1.0F;
        StartCoroutine(ChangeDirection());
    }
}
