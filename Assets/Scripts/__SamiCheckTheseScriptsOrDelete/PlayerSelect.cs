﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent( typeof( AudioSource ) )]
public class PlayerSelect : MonoBehaviour
{
	public int m_iPlayerIndex;
	public float m_fFlashSpeed = 30.0f;
	public AudioClip m_oSelectionSound;
	private Renderer m_oRenderer;
	private Vector3 m_oPositionInit;
	private int m_iLevelSelected = -1;
	private float m_fTSelect;
	public float m_fLoadProgress;
	private Menu m_oMenu;
	private bool m_bPlayerReady;
	private float m_fTPlayerRead;
	private bool m_bCanLoad;
	private ParticleSystem m_oParticleSystemLoad;
	
	void Awake()
	{
		m_oRenderer = GetComponent<Renderer>();
		m_oPositionInit = transform.position;

		GetComponent<AudioSource>().Play ();

		DeselectLevel();
		EnableLoading();
	}
	void Start()
	{
        m_oParticleSystemLoad = this.GetComponentInChildren<ParticleSystem>();
	}
	void Update()
	{
		if( m_bCanLoad && !m_bPlayerReady )
		{
			m_fLoadProgress = ( Time.time - m_fTSelect ) / m_oMenu.GetLoadDuration();
            if (m_fLoadProgress >= 1.0f)
            {
                m_bPlayerReady = true;
                m_fTPlayerRead = Time.time;
                m_oParticleSystemLoad.Play();
            }
            else
            {
                m_oParticleSystemLoad.Stop();
            }
		}
        float fTexOffY = 1.0F;

        if (m_fLoadProgress < 1.0f)
            fTexOffY = -0.5f + m_fLoadProgress;
        else
            fTexOffY = 0.5f; //+ Mathf.Sin((Time.time - m_fTPlayerRead) * m_fFlashSpeed); //  Random.Range( -10.0f, 10.0f );
        
            m_oRenderer.material.SetTextureOffset("_MainTex", new Vector2(0.0f, -fTexOffY));
		//m_oRenderer.material.SetColor( "_Color", m_oMenu.DoesLevelSelectionMatch() ? Color.white : Color.red );
		
		Vector2 oDir = SerialInput.GetDir( m_iPlayerIndex );
		bool bLeft = ( oDir.x < 0.0f );
		bool bRight = ( oDir.x > 0.0f );
		bool bDown = ( oDir.y < 0.0f );
		bool bUp = ( oDir.y > 0.0f );
		
		if( ( ( bLeft || bRight ) && ( bDown || bUp ) ) // diagonal
		   || !( bLeft || bRight || bDown || bUp ) ) // no direction
		{
			DeselectLevel();
			oDir = Vector2.zero;
		}
		else
		{
			if( bLeft )
			{
				UpdateSelectedLevel( 0 );
			}
			else if( bRight )	
			{
				UpdateSelectedLevel( 1 );
			}
			if( bDown )
			{
				UpdateSelectedLevel( 2 );
			}
			else if( bUp )
			{
				UpdateSelectedLevel( 3 );
			}
		}
		
		transform.position = m_oPositionInit + new Vector3( oDir.x, oDir.y, 0.0f ) * 0.5f;

        Vector3 tempControllerRotation = new Vector3(90 - (30 * oDir.y), 180 - (30 * oDir.x), 0);
        transform.parent.Find("mainmenue_instructions").rotation = Quaternion.Euler(tempControllerRotation);

	}
	public void Init( Menu oMenu )
	{
		m_oMenu = oMenu;
	}
	public int GetSelectedLevel()
	{
		return m_iLevelSelected;
	}
	public float GetLoadProgress()
	{
		return m_fLoadProgress;
	}
	public void ResetLoadProgress()
	{
		m_fTSelect = Time.time;
		m_fLoadProgress = 0.0f;
		m_bPlayerReady = false;
	}
	public bool IsPlayerReady()
	{
		return m_bPlayerReady;
	}
	public void EnableLoading()
	{
		m_bCanLoad = true;
	}
	public void DisableLoading()
	{
		m_bCanLoad = false;
	}
	public void DeselectLevel()
	{
		m_iLevelSelected = -1;
		m_fTSelect = -1.0f;
		m_fLoadProgress = 0.0f;
		m_bPlayerReady = false;
	}
	private void UpdateSelectedLevel( int iLevel )
	{
		if( iLevel != m_iLevelSelected )
		{
			GetComponent<AudioSource>().PlayOneShot( m_oSelectionSound, 0.3f );
			m_iLevelSelected = iLevel;
			ResetLoadProgress();
		}
	}
}
