﻿using UnityEngine;
using System.Collections;

public class RainbowColor : MonoBehaviour {

	public Color32[] switchingColors;
	private int currentColorID;
	public Color currentColor;
	
	public float timeNeeded;

	// Use this for initialization
	void Start () {
		StartCoroutine(ChangeColor());
	}
	
	// Update is called once per frame
	void Update () {

        //iTween.ColorUpdate(this.gameObject, iTween.Hash("color", currentColor,
        //                                                        "time", timeNeeded));

        this.GetComponent<UnityEngine.UI.Image>().CrossFadeColor(currentColor, timeNeeded, false, false);
																
	}
	
	private IEnumerator ChangeColor(){
	
		
		
		currentColorID ++;
		
		if(currentColorID >= switchingColors.Length){
            currentColorID = 0;
		}

        currentColor = switchingColors[currentColorID];
		
		yield return new WaitForEndOfFrame();

        yield return new WaitForSeconds(timeNeeded);

		RestartTimer();
	}
	
	private void RestartTimer(){
		StartCoroutine(ChangeColor());
	}
}
