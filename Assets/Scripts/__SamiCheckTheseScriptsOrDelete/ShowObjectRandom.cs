﻿using UnityEngine;
using System.Collections;

public class ShowObjectRandom : MonoBehaviour {

    public Mesh[] allObjects;

    public Vector3 normalPosition;
    public Vector3 hiddenPosition;

    public Vector2 randomWaitRange;
     
    private int oldItem; //Not implemented yet

	// Use this for initialization
	void Start () {
	    this.GetComponent<MeshFilter>().mesh = allObjects[Random.Range(0,allObjects.Length)];
        GenerateNewObject();
	}

    private void Hide()
    {
        iTween.MoveTo(this.gameObject, iTween.Hash("position", hiddenPosition,
                                "time", 1.2F,
                                "easetype", iTween.EaseType.easeInOutSine,
                                "islocal", true,
            "oncomplete", "GenerateNewObject"));
    }

    private void GenerateNewObject()
    {
        //Get the new
        this.GetComponent<MeshFilter>().mesh = allObjects[Random.Range(0, allObjects.Length)];

        iTween.MoveTo(this.gameObject, iTween.Hash("position", normalPosition,
                                "time", 1.2F,
                                "islocal", true,    
                                "easetype", iTween.EaseType.easeInOutSine
            ));

        StartCoroutine(WaitForNextSwitch());
    }

    private IEnumerator WaitForNextSwitch()
    {
        yield return new WaitForSeconds(Random.Range(randomWaitRange.x, randomWaitRange.y));

        Hide();
    }
    
}
