﻿using UnityEngine;
using System.Collections;

public class tile : MonoBehaviour {

    public float scrollSpeed = 0.5F;
	
	// Update is called once per frame
	void Update () {

        float offset = Time.time * scrollSpeed;
        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(offset, offset));
	}
}
