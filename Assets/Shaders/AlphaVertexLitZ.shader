﻿Shader "Transparent/VertexLit with Z" {

Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
}
 
SubShader {
    Tags { "Queue" = "Transparent"}
    LOD 200
    // Render into depth buffer only
   
    // Render normally
    Pass {
    	//Cull Back
        //ZWrite Off
        //ZTest LEqual
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask RGB
        Material {
            Diffuse [_Color]
            Ambient [_Color]
        }
        Lighting On
        SetTexture [_MainTex] {
            Combine texture * primary DOUBLE, texture * primary
        } 
        
    }
    
    
}
}